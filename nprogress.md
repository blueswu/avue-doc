<script>
export default {

}
</script>

# NProgress 进度条

:::tip
 2.0.5+
::::


:::demo 
```html
<div style="width:400px">
 <el-button type="primary" @click="$NProgress.start()">开始</el-button>
 <el-button type="danger" @click="$NProgress.done()">停止</el-button>
 <el-button type="warning" @click="$NProgress.remove()">删除</el-button>
</div>
<script>
export default {

}
</script>

```
:::


