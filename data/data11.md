<script>
export default {
  data(){
  return {
      option: {
        span:8,
        data: [
            {
              click: function (item) {
                alert(JSON.stringify(item));
              },
              title: 'New Visits',
              count: '102,400',
              icon: 'el-icon-message',
              color: '#00a7d0',
            },
            {
              click: function (item) {
                alert(JSON.stringify(item));
              },
              title: 'Messages',
              count: '81,212',
              icon: 'el-icon-info',
              color: 'rgb(27, 201, 142)',
            },
            {
              click: function (item) {
                alert(JSON.stringify(item));
              },
              title: 'Purchases',
              count: '9,280',
              icon: 'el-icon-success',
              color: 'rgb(230, 71, 88)',
            }
          ]
      }
    }
  }
}
</script>
# DataPanel 数据展示

:::tip
 1.0.5+
::::

:::demo 
```html
<avue-data-panel :option="option"></avue-data-panel>
<script>
export default {
  data(){
  return {
      option: {
        span:8,
        data: [
            {
              click: function (item) {
                alert(JSON.stringify(item));
              },
              title: 'New Visits',
              count: '102,400',
              icon: 'el-icon-message',
              color: '#00a7d0',
            },
            {
              click: function (item) {
                alert(JSON.stringify(item));
              },
              title: 'Messages',
              count: '81,212',
              icon: 'el-icon-info',
              color: 'rgb(27, 201, 142)',
            },
            {
              click: function (item) {
                alert(JSON.stringify(item));
              },
              title: 'Purchases',
              count: '9,280',
              icon: 'el-icon-success',
              color: 'rgb(230, 71, 88)',
            }
          ]
      }
    }
  }
}
</script>

```
:::
