<script>
export default {
  data(){
    return {
      option: {
        span:6,
        data: [
        {
            click: function (item) {
              alert(JSON.stringify(item));
            },
            count: 100,
            title: '日活跃数',
            href:'https://avuejs.com',
            target:'_blank'
          },
          {
            click: function (item) {
              alert(JSON.stringify(item));
            },
            count: '3,000',
            title: '月活跃数',
            href:'https://avuejs.com',
            target:'_blank'
          },
          {
            click: function (item) {
              alert(JSON.stringify(item));
            },
            count: '20,000',
            title: '年活跃数',
            href:'https://avuejs.com',
            target:'_blank'
          },
          {
            click: function (item) {
              alert(JSON.stringify(item));
            },
            count: '40,000',
            title: '周活跃数',
            href:'https://avuejs.com',
            target:'_blank'
          }
        ]
      },
    }
  }
}
</script>

# DataDisplay 数据展示

:::tip
 1.0.0+
::::

:::demo 
```html
<avue-data-display :option="option"></avue-data-display>
<script>
export default {
  data(){
    return {
      option: {
        span:6,
        data: [
        {
            click: function (item) {
              alert(JSON.stringify(item));
            },
            count: 100,
            title: '日活跃数',
            href:'https://avuejs.com',
            target:'_blank'
          },
          {
            click: function (item) {
              alert(JSON.stringify(item));
            },
            count: '3,000',
            title: '月活跃数',
            href:'https://avuejs.com',
            target:'_blank'
          },
          {
            click: function (item) {
              alert(JSON.stringify(item));
            },
            count: '20,000',
            title: '年活跃数',
            href:'https://avuejs.com',
            target:'_blank'
          },
          {
            click: function (item) {
              alert(JSON.stringify(item));
            },
            count: '40,000',
            title: '周活跃数',
            href:'https://avuejs.com',
            target:'_blank'
          }
        ]
      },
    }
  }
}
</script>

```
:::
