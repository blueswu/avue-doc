# dataType说明

dataType顾名思义就是改变数据类型的，比如说如下场景，
- 我们表单中的checkbox的数据类型为数组。
- 正常数据格式为数组[1,2,3,4]。
- 后台返回的类型为用逗号隔开的字符串1,2,3,4。
- 这个时候你只要配置dataType属性即可自动完成转化。

## 类型
- string
- number

## 组件
- cascader
- checkbox
- upload
- select(多选)
- tree(多选)
- img
- array