<script>
export default{
  data() {
    return {
      form: {
          video:'/i/movie.ogg',
          imgUrl:[
            { "label": "avue@226d5c1a_image.png", "value": "/images/logo-bg.jpg" },
            {"label": "avue@226d5c1a_video.png", "value": 'https://www.w3school.com.cn/i/movie.ogg'}
          ],
          imgUrl3:'/images/logo-bg.jpg',
          string:'/images/logo-bg.jpg,/images/logo-bg.jpg',
          img:['/images/logo-bg.jpg','/images/logo-bg.jpg']
      },
      option: {
        labelWidth: 120,
        column: [
          {
            label: '视频',
            prop: 'video',
            type: 'upload',
            listType: 'picture-img',
            propsHttp: {
              home: 'https://www.w3school.com.cn'
            },
            data:{
              params:1
            },
            span: 24,
            tip: '只能上传jpg/png用户头像，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '附件上传',
            prop: 'imgUrl',
            type: 'upload',
            loadText: '附件上传中，请稍等',
            span: 24,
            propsHttp: {
              res: 'data'
            },
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '水印头像',
            prop: 'imgUrl3',
            type: 'upload',
            listType: 'picture-img',
            span: 24,
            propsHttp: {
              res: 'data'
            },
            canvasOption: {
              text: 'avue',
              ratio: 0.1
            },
            tip: '只能上传jpg/png用户头像，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '照片墙',
            prop: 'imgUrl',
            type: 'upload',
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            propsHttp: {
              res: 'data'
            },
            action: '/imgupload'
          },
          {
            label: '数组图片组',
            prop: 'img',
            dataType: 'array',
            type: 'upload',
            propsHttp: {
              res: 'data.0'
            },
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '字符串图片组',
            prop: 'string',
            dataType: 'string',
            type: 'upload',
            propsHttp: {
              res: 'data'
            },
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '拖拽上传',
            prop: 'imgUrl',
            type: 'upload',
            span: 24,
            drag: true,
            propsHttp: {
              res: 'data'
            },
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '缩略图上传',
            prop: 'imgUrl',
            type: 'upload',
            limit: 3,
            span: 24,
            propsHttp: {
              res: 'data'
            },
            listType: 'picture',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          }
        ]
      }
    }
  },
  methods: {
    uploadDelete(column,file) {
      console.log(column,file)
      return this.$confirm(`这里是自定义的，是否确定移除该选项？`);
    },
    uploadBefore(file, done, loading,column) {
      console.log(file,column)
      //如果你想修改file文件,由于上传的file是只读文件，必须复制新的file才可以修改名字，完后赋值到done函数里,如果不修改的话直接写done()即可
      var newFile = new File([file], '1234', { type: file.type });
      done(newFile)
      this.$message.success('上传前的方法')
    },
    uploadAfter(res, done, loading,column) {
      console.log(res,column)
      done()
      this.$message.success('上传后的方法')
    },
    uploadError(error, column) {
      this.$message.success('上传失败')
      console.log(error, column)
    },
    uploadPreview(file,column,done){
      console.log(file,column)
      done()//默认执行打开方法
      this.$message.success('自定义查看方法,查看控制台')
    },
    submit() {
      this.$message.success('当前数据' + JSON.stringify(this.form))
    }
  }
}
</script>

# 图片上传
```
- 如果你想修改file文件,由于上传的file是只读文件，
- 必须复制新的file才可以修改名字，完后赋值到done函数里
- 如果不修改的话直接写done()即可,可以参考uploadBefore函数
- 阿里云和七牛云同理使用
```

:::demo  提供了`dataType`参数可以去配置存到数据库的结构体类型,可以配置`canvasOption`属性去生成水印和压缩图片，也可配`props`和`propsHttp`属性去加载不用的结构体中的`key`和`value`,具体参数配置参考下面demo和文档
```html
<avue-form :option="option" v-model="form" :upload-preview="uploadPreview" 
:upload-error="uploadError" :upload-delete="uploadDelete" :upload-before="uploadBefore" :upload-after="uploadAfter"> </avue-form>

<script>
export default{
  data() {
    return {
      form: {
          video:'/i/movie.ogg',
          imgUrl:[
            { "label": "avue@226d5c1a_image.png", "value": "/images/logo-bg.jpg" },
            {"label": "avue@226d5c1a_video.png", "value": 'https://www.w3school.com.cn/i/movie.ogg'}
          ],
          imgUrl3:'/images/logo-bg.jpg',
          string:'/images/logo-bg.jpg,/images/logo-bg.jpg',
          img:['/images/logo-bg.jpg','/images/logo-bg.jpg']
      },
      option: {
        labelWidth: 120,
        column: [
          {
            label: '视频',
            prop: 'video',
            type: 'upload',
            propsHttp: {
              home: 'https://www.w3school.com.cn'
            },
            listType: 'picture-img',
            span: 24,
            tip: '只能上传jpg/png用户头像，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '附件上传',
            prop: 'imgUrl',
            type: 'upload',
            loadText: '附件上传中，请稍等',
            span: 24,
            propsHttp: {
              res: 'data'
            },
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '水印头像',
            prop: 'imgUrl3',
            type: 'upload',
            listType: 'picture-img',
            span: 24,
            propsHttp: {
              res: 'data'
            },
            canvasOption: {
              text: 'avue',
              ratio: 0.1
            },
            tip: '只能上传jpg/png用户头像，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '照片墙',
            prop: 'imgUrl',
            type: 'upload',
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            propsHttp: {
              res: 'data'
            },
            action: '/imgupload'
          },
          {
            label: '数组图片组',
            prop: 'img',
            dataType: 'array',
            type: 'upload',
            propsHttp: {
              res: 'data.0'
            },
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '字符串图片组',
            prop: 'string',
            dataType: 'string',
            type: 'upload',
            propsHttp: {
              res: 'data'
            },
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '拖拽上传',
            prop: 'imgUrl',
            type: 'upload',
            span: 24,
            drag: true,
            propsHttp: {
              res: 'data'
            },
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '缩略图上传',
            prop: 'imgUrl',
            type: 'upload',
            limit: 3,
            span: 24,
            propsHttp: {
              res: 'data'
            },
            listType: 'picture',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          }
        ]
      }
    }
  },
  methods: {
    uploadDelete(column,file) {
      console.log(column,file)
      return this.$confirm(`这里是自定义的，是否确定移除该选项？`);
    },
    uploadBefore(file, done, loading,column) {
      console.log(file,column)
      //如果你想修改file文件,由于上传的file是只读文件，必须复制新的file才可以修改名字，完后赋值到done函数里,如果不修改的话直接写done()即可
      var newFile = new File([file], '1234', { type: file.type });
      done(newFile)
      this.$message.success('上传前的方法')
    },
    uploadError(error, column) {
      this.$message.success('上传失败')
      console.log(error, column)
    },
    uploadAfter(res, done, loading,column) {
      console.log(res,column)
      done()
      this.$message.success('上传后的方法')
    },
    uploadPreview(file,column,done){
      console.log(file,column)
      done()//默认执行打开方法
      this.$message.success('自定义查看方法,查看控制台')
    },
    submit() {
      this.$message.success('当前数据' + JSON.stringify(this.form))
    }
  }
}
</script>


```
:::

## Variables

|参数|说明|类型|可选值|默认值|
|----------------------|--------------------------------|--------|------------|------|
|upload-before|图片上传前的回调,会暂停图片上传function(file,done,loading)，done用于继续图片上传，loading用于中断操作|Function|—|—|
|upload-after|图片上传后的回调,function(res,done)，done用于结束操作，loading用于中断操作|Function|—|—|
|upload-delete|删除前的回调,返回true/false即可或promise对象function(column)|Function|—|—|
|upload-preview|查看前的回调function(file,column)|Function|—|—|
|upload-error|上传失败错误回调function(error,column)|Function|—|—|

## props Attributes
#### 如果dataType为arrat和string时，不存在该参数的设置
|参数|说明|类型|可选值|默认值|
|--------|------------------|------|------|------|
|label|数据对象的图片地址|String|—|label|
|value|数据对象的图片名称|String|—|value|

## propsHttp Attributes
#### 如果dataType为arrat和string时，不存在name字段的配置
|参数|说明|类型|可选值|默认值|
|--------|------------------|------|------|------|
|home|图片的根路径地址，例如返回data:{url:'/xxxx.jpg',name:''},home属性为http://xxx.com/,则最终的图片显示地址为http://xxx.com/xxxx.jpg|String|—|url|
|res|返回结构体的层次，例如返回data:{url:'',name:''},则res配置为data|String|—|-|
|url|上传成功返回结构体的图片地址，例如返回data:{urlsrc:'',name:''},则url配置为urlsrc|String|—|url|
|name|上传成功返回结构体的图片名称，例如返回data:{urlsrc:'',namesrc:''},则name配置为namesrc，当listType为picture-img属性不存在,|String|—|name|
|fileName|上传文件流时的名称|String|—|file|
|data|上传携带参数|Object|—|-|
|headers|上传携带头部|Object|—|-|

## canvasOption Attributes

|参数|说明|类型|可选值|默认值|
|--------|------------------|------|------|------|
|text|字体的文字|String|—|avuejs.com|
|fontFamily|字体类型|String|—|microsoft yahei|
|color|字体的颜色|String|—|#999|
|fontSize|字体的大小|String|—|16|
|opacity|文字的透明度|String|—|100|
|bottom|文字距离图片底部的距离|String|—|10|
|right|文字距离图片右边的距离|String|—|10|
|ratio|压缩图片比率|String|0-1(可以是小数)|1|

