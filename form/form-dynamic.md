<script>
export default {
  data() {
      return {
        obj: {
          dynamic: [{
            input: 1,
            select: 1,
            radio: 1,
          }, {
            input: 2,
            select: 2,
            radio: 1,
          }]
        },
        option: {
          labelWidth: 110,
          column: [{
            label: '输入框',
            prop: "input",
            span:12,
            row: true
          },
          {
            label: '子表单',
            prop: 'dynamic',
            type: 'dynamic',
            span:24,
            children: {
              align: 'center',
              headerAlign: 'center',
              rowAdd:(done)=>{
                this.$message.success('新增回调');
                  done({
                    input:'默认值'
                  });
              },
              rowDel:(row,done)=>{
                this.$message.success('删除回调'+JSON.stringify(row));
                done();
              },
              column: [{
                width: 200,
                label: '输入框',
                prop: "input",
                formslot: true,
              }, {
                width: 200,
                label: '选择框',
                prop: "select",
                type: 'select',
                rules:[{
                  type:'number',
                  require:true,
                  message:'请选择选择框',
                }],
                dicData: [{
                  label: '测试1',
                  value: 1
                }, {
                  label: '测试2',
                  value: 2
                }]
              }, {
                width: 200,
                label: '多选',
                prop: "checkbox",
                type: 'checkbox',
                dicData: [{
                  label: '测试1',
                  value: 1
                }, {
                  label: '测试2',
                  value: 2
                }]
              }, {
                width: 200,
                label: '开关',
                prop: "switch",
                type: 'switch',
                dicData: [{
                  label: '测试1',
                  value: 1
                }, {
                  label: '测试2',
                  value: 2
                }]
              }, {
                width: 200,
                label: '数字框',
                prop: "number",
                type: 'number'
              }]
            }
          },

          ]
        }
      }
  }
}
</script>
# 表单子表单

:::tip
 2.1.0+
::::

:::demo 配置`dynamic`的`children`字段即可，配置方法和`crud`组件一致,有`rowAdd`和`rowDel`回调函数,与`form`组件的卡槽用法一致
```html
<avue-form :option="option" v-model="obj">
 <template slot-scope="{row}" slot="input">
    <el-tag>{{row}}</el-tag>
  </template>
</avue-form>
<script>
export default {
  data() {
      return {
        obj: {
          dynamic: [{
            input: 1,
            select: 1,
            radio: 1,
          }, {
            input: 2,
            select: 2,
            radio: 1,
          }]
        },
        option: {
          labelWidth: 110,
          column: [{
            label: '输入框',
            prop: "input",
            span:12,
            row: true
          },
          {
            label: '子表单',
            prop: 'dynamic',
            type: 'dynamic',
            span:24,
            children: {
              align: 'center',
              headerAlign: 'center',
              rowAdd:(done)=>{
                this.$message.success('新增回调');
                  done({
                    input:'默认值'
                  });
              },
              rowDel:(row,done)=>{
                this.$message.success('删除回调'+JSON.stringify(row));
                done();
              },
              column: [{
                width: 200,
                label: '输入框',
                prop: "input",
                formslot: true,
              }, {
                width: 200,
                label: '选择框',
                prop: "select",
                type: 'select',
                rules:[{
                  type:'number',
                  require:true,
                  message:'请选择选择框',
                }],
                dicData: [{
                  label: '测试1',
                  value: 1
                }, {
                  label: '测试2',
                  value: 2
                }]
              }, {
                width: 200,
                label: '多选',
                prop: "checkbox",
                type: 'checkbox',
                dicData: [{
                  label: '测试1',
                  value: 1
                }, {
                  label: '测试2',
                  value: 2
                }]
              }, {
                width: 200,
                label: '开关',
                prop: "switch",
                type: 'switch',
                dicData: [{
                  label: '测试1',
                  value: 1
                }, {
                  label: '测试2',
                  value: 2
                }]
              }, {
                width: 200,
                label: '数字框',
                prop: "number",
                type: 'number'
              }]
            }
          },

          ]
        }
      }
  }
}
</script>

```
:::

