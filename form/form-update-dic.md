<script>
export default {
  data(){
    return {
        form:{},
        option1:{
          column: [{
            label: '下拉',
            prop:'select',
            span:24,
            type:'select',
          }]
        },
        option:{
          column: [{
            label: '字典',
            span:24,
            type:'radio',
            prop: 'radio',
            dicUrl: 'https://cli.avuejs.com/api/area/getProvince',
            props: {
              label: 'name',
              value: 'code'
            }
          }]
        }, 
        option2:{
          column: [{
            label: '字典',
            span:24,
            type:'radio',
            prop: 'radio',
            dicUrl: 'https://cli.avuejs.com/api/area/getProvince',
            props: {
              label: 'name',
              value: 'code'
            }
          }]
        }
    }
  },
  methods:{
    updateDic(){
        this.$refs.form.updateDic('radio',[{
          name:'字典1',
          code:1
        },{
          name:'字典0',
          code:2
        }])
    },
    updateUrlDic(){
       var form = this.$refs.form2
       this.$message.success('先设置本地字典1s后请求url')
       form.updateDic('radio',[{
          name:'字典1',
          code:1
        },{
          name:'字典0',
          code:2
        }]);
        setTimeout(()=>{
          form.updateDic('radio');
        },1000)
    },
    updateOption(){
       var column = this.findObject(this.option1.column,'select');
       column=Object.assign(column,{
         type:'radio',
         label:'单选框',
         dicData:[{
          label:'字典1',
          value:1
          },{
            label:'字典0',
            value:2
          }]
       })
    }
  }
}
</script>


# 字典动态赋值
- crud和form的操作方法一至,调用内置的updateDic方法

:::tip
 1.0.11+
::::


## 更新字典
:::demo  crud和form的操作方法一至,调用内置的`updateDic`方法
```html
<el-button  type="primary" size="small" @click="updateDic">点我更新字典</el-button><br /><br />
<avue-form ref="form" :option="option" v-model="form"></avue-form>
<script>
export default {
  data(){
    return {
        form:{},
        option:{
          column: [{
            label: '字典',
            span:24,
            type:'radio',
            prop: 'radio',
            dicUrl: 'https://cli.avuejs.com/api/area/getProvince',
            props: {
              label: 'name',
              value: 'code'
            }
          }]
        }
    }
  },
  methods:{
    updateDic(){
        this.$refs.form.updateDic('radio',[{
          name:'字典1',
          code:1
        },{
          name:'字典0',
          code:2
        }])
    }
  }
}
</script>

```
:::

## 更新网络字典
:::demo  和上面方法一样，只是再调用`updateDic`时不需要传新的字典，他会自己调用`dicUrl`去请求字典
```html
<el-button  type="primary" size="small" @click="updateUrlDic">点我更新字典</el-button><br /><br />
<avue-form ref="form2" :option="option2" v-model="form"></avue-form>
<script>
export default {
  data(){
    return {
        form:{},
        option2:{
          column: [{
            label: '字典',
            span:24,
            type:'radio',
            prop: 'radio',
            dicUrl: 'https://cli.avuejs.com/api/area/getProvince',
            props: {
              label: 'name',
              value: 'code'
            }
          }]
        }
    }
  },
  methods:{
   
    updateUrlDic(){
      var form = this.$refs.form2
       this.$message.success('先设置本地字典1s后请求url')
       form.updateDic('radio',[{
          name:'字典1',
          code:1
        },{
          name:'字典0',
          code:2
        }]);
        setTimeout(()=>{
          form.updateDic('radio');
        },1000)
    },
  }
}
</script>

```
:::


## 查找列的配置
:::demo  调用内置方法`findColumnIndex`查找对应`prop`的属性序号 ,同时你也可以更新字典
```html
<el-button  type="primary" size="small" @click="updateOption">点我更改配置</el-button><br /><br />
<avue-form ref="form1" :option="option1" v-model="form"></avue-form>
<script>
export default {
  data(){
    return {
        form:{},
        option1:{
          column: [{
            label: '下拉',
            prop:'select',
            span:24,
            type:'select',
          }]
        }
    }
  },
  methods:{
    updateOption(){
       var column = this.findObject(this.option1.column,'select');
       column=Object.assign(column,{
         type:'radio',
         label:'单选框',
         dicData:[{
          label:'字典1',
          value:1
          },{
            label:'字典0',
            value:2
          }]
       })
    }
  }
}
</script>

```
:::





