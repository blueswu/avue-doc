<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
export default{
  data() {
      return {
        obj: {
          province: 110000
        },
        option: {
          column: [
            {
              label: '省份',
              prop: 'province',
              type: 'select',
              dataType:'number',
              props: {
                label: 'name',
                value: 'code'
              },
              dicUrl: `${baseUrl}/getProvince`,
              rules: [
                {
                  required: true,
                  message: '请选择省份',
                  trigger: 'blur'
                }
              ]
            }
          ]
        }
      }
    },
}
</script>

# 数据类型
:::tip
 1.0.11+
::::

## 普通用法 
:::demo  解决数据字典和字段类型不匹配问题,配置`dataType`属性(string / number)
```html
<avue-form :option="option" v-model="obj" > </avue-form>

<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
export default{
  data() {
      return {
        obj: {
          province: 110000
        },
        option: {
          column: [
            {
              label: '省份',
              prop: 'province',
              type: 'select',
              dataType:'number',
              props: {
                label: 'name',
                value: 'code'
              },
              dicUrl: `${baseUrl}/getProvince`,
              rules: [
                {
                  required: true,
                  message: '请选择省份',
                  trigger: 'blur'
                }
              ]
            }
          ]
        }
      }
    },
}
</script>

```
:::
