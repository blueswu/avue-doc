<script>
export default {
   data(){
     return {
       form:{},
       option: {
          labelWidth: 120,
          column: [
            {
              label: '七牛上传',
              prop: 'imgUrl',
              type: 'upload',
              listType: 'picture-img',
              propsHttp: {
                name: 'hash',
                url: "key"
              },
              oss: 'qiniu',
              loadText: '附件上传中，请稍等',
              span: 24,
              tip: '只能上传jpg/png文件，且不超过500kb',
            }
          ]
        }
     }
   }
}
</script>

# 七牛云oss上传



```
<!-- 导入需要的包 -->
<script src="https://avuejs.com/cdn/CryptoJS.js"></script>
<!-- 并且需要入口处全局配置七牛云的参数 -->
Vue.use(window.AVUE, {
  qiniu: {
    AK: '',//七牛云相关密钥
    SK: '',//七牛云相关密钥
    bucket:'https://upload.qiniup.com'//存储地区，默认为华东，其他的如下表
    scope: 'test',//存储空间名称
    url: 'https://cdn.avuejs.com/'//外链的域名地址
  }
})

华东——http(s)://upload.qiniup.com
华北——http(s)://upload-z1.qiniup.com
华南——http(s)://upload-z2.qiniup.com
北美——http(s)://upload-na0.qiniup.com
东南亚——http(s)://upload-as0.qiniup.com
```

:::tip
 1.0.6+
::::


:::demo 
```html
<avue-form :option="option" v-model="form"> </avue-form>
<script>
export default {
   data(){
     return {
       form:{},
       option: {
          labelWidth: 120,
          column: [
            {
              label: '七牛上传',
              prop: 'imgUrl',
              type: 'upload',
              listType: 'picture-img',
              propsHttp: {
                name: 'hash',
                url: "key"
              },
              oss: 'qiniu',
              loadText: '附件上传中，请稍等',
              span: 24,
              tip: '只能上传jpg/png文件，且不超过500kb',
            }
          ]
        }
     }
   }
}
</script>

```
:::

