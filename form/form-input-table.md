<script>
export default{
  data() {
    return {
      form: { 
        table:'0'
      },
      option: {
          column: [
            {
              label: '表格',
              prop: 'table',
              type: 'table',
              children: {
                border: true,
                column: [{
                  label: '姓名',
                  width: 120,
                  search:true,
                  searchSpan:24,
                  prop: 'name'
                }, {
                  label: '性别',
                  prop: 'sex'
                }],
              },
              formatter: (row) => {
                return row.name + '-' + row.sex
              },
              onLoad: ({ page,value,data }, callback) => {
                //首次加载去查询对应的值
                if (value) {
                  this.$message.success('首次查询'+value)
                  callback({
                    id: '0',
                    name: '张三',
                    sex: '男'
                  })
                  return
                }
                if(data){
                  this.$message.success('搜索查询参数'+JSON.stringify(data))
                }
                if(page){
                  this.$message.success('分页参数'+JSON.stringify(page))
                }
                //分页查询信息
                callback({
                  total: 2,
                  data: [{
                    id: '0',
                    name: '张三',
                    sex: '男'
                  }, {
                    id: '1',
                    name: '李四',
                    sex: '女'
                  }]
                })
              },
              props: {
                label: 'name',
                value: 'id'
              },
              rules: [
                {
                  required: true,
                  message: '请选择省份',
                  trigger: 'blur'
                }
              ]
            }]
        }
    }
  }
}
</script>
# 表格选择器


```  
内部组件为crud组件，大部分属性还是支持的
```

## 普通用法 
:::demo  
```html
<avue-form :option="option" v-model="form" ></avue-form>

<script>
export default{
  data() {
    return {
      form: { 
        table:'0'
      },
      option: {
          column: [
            {
              label: '表格',
              prop: 'table',
              type: 'table',
              children: {
                border: true,
                column: [{
                  label: '姓名',
                  width: 120,
                  search:true,
                  searchSpan:24,
                  prop: 'name'
                }, {
                  label: '性别',
                  prop: 'sex'
                }],
              },
              formatter: (row) => {
                return row.name + '-' + row.sex
              },
              onLoad: ({ page, value,data }, callback) => {
                //首次加载去查询对应的值
                if (value) {
                  this.$message.success('首次查询'+value)
                  callback({
                    id: '0',
                    name: '张三',
                    sex: '男'
                  })
                  return
                }
                if(data){
                  this.$message.success('搜索查询参数'+JSON.stringify(data))
                }
                if(page){
                  this.$message.success('分页参数'+JSON.stringify(page))
                }
                //分页查询信息
                callback({
                  total: 2,
                  data: [{
                    id: '0',
                    name: '张三',
                    sex: '男'
                  }, {
                    id: '1',
                    name: '李四',
                    sex: '女'
                  }]
                })
              },
              props: {
                label: 'name',
                value: 'id'
              },
              rules: [
                {
                  required: true,
                  message: '请选择省份',
                  trigger: 'blur'
                }
              ]
            }]
        }
    }
  }
}
</script>

```
:::
