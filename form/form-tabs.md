<script>
export default {
  data(){
    return {
       tabs:true,
       form:{
          text:'文本',
          text1:'文本1',
          text2:'文本2',
          text3:'文本3',
       },
    }
  },
  computed:{
    option(){
      return {
          tabs:this.tabs,
          tabsActive:2,
          column: [{
            label: '内容1',
            prop: 'text1',
          }],
          group:[
            {
              icon:'el-icon-info',
              label: '分组1',
              prop: 'group1',
              column: [{
                label: '内容1',
                prop: 'text1',
              }]
            },{
              icon:'el-icon-info',
              label: '分组2',
              prop: 'group2',
              column: [{
                label: '选项卡2',
                prop: 'text2',
              }, {
                label: '选项卡3',
                prop: 'text3',
              }]
            }
          ]
       }
    }
  },
  created(){
    this.option1 = Object.assign(this.deepClone(this.option),{
      card:true
    })
  },
  methods:{
    handleSubmit(form){
       this.$message.success(JSON.stringify(this.form))
    }
  }
}
</script>
# 表单选项卡
可以根据输入的内容不同划分不用的分组

:::demo  用法普通的form分组用法一样，配置`tabs`为true即可开启选项卡
```html
<el-button @click="tabs=!tabs">转化</el-button><br /><br />
<avue-form :option="option" v-model="form" @submit="handleSubmit">
  <template slot="group1Header">
    <h4>自定义表头</h4>
  </template>
</avue-form>
<script>
export default {
  data(){
    return {
       tabs:true,
       form:{
          text:'文本',
          text1:'文本1',
          text2:'文本2',
          text3:'文本3',
       },
    }
  },
  computed:{
    option(){
      return {
          tabs:this.tabs,
          tabsActive:2,
          column: [{
            label: '内容1',
            prop: 'text1',
          }],
          group:[
            {
              icon:'el-icon-info',
              label: '分组1',
              prop: 'group1',
              column: [{
                label: '内容1',
                prop: 'text1',
              }]
            },{
              icon:'el-icon-info',
              label: '分组2',
              prop: 'group2',
              column: [{
                label: '选项卡2',
                prop: 'text2',
              }, {
                label: '选项卡3',
                prop: 'text3',
              }]
            }
          ]
       }
    }
  },
  created(){
    this.option1 = Object.assign(this.deepClone(this.option),{
      card:true
    })
  },
  methods:{
    handleSubmit(form){
       this.$message.success(JSON.stringify(this.form))
    }
  }
}
</script>

```
:::