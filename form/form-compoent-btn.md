<script>
const DIC=[{
  label:'男',
  value:0
},{
  label:'女',
  value:1
},{
  label:'未知',
  value:''
}]
export default {
    data() {
      return {
        obj: {
          radio1:0,
          radio2:0,
          checkbox1:[0,1],
          checkbox2:[0,1]
        },
        option: {
          column: [
            {
              label: '实心单选',
              prop: 'radio1',
              type: 'radio',
              span:24,
              button:true,
              dicData:DIC
            },{
              label: '空心单选',
              prop: 'radio2',
              type: 'radio',
              span:24,
              border:true,
              dicData:DIC
            },{
              label: '实心多选',
              prop: 'checkbox1',
              type: 'checkbox',
              span:24,
              button:true,
              dicData:DIC
            },{
              label: '空心多选',
              prop: 'checkbox2',
              span:24,
              type: 'checkbox',
              border:true,
              dicData:DIC
            }
          ]
        }
      }
    }
}
</script>
# 表单单选多选类型

:::tip
 2.1.0+
::::

:::demo  自由组合`button`和`border`可以达到不同的样式效果
```html
<avue-form :option="option" v-model="obj"></avue-form>
<script>
const DIC=[{
  label:'男',
  value:0
},{
  label:'女',
  value:1
},{
  label:'未知',
  value:''
}]
export default {
    data() {
      return {
        obj: {
          radio1:0,
          radio2:0,
          checkbox1:[0,1],
          checkbox2:[0,1]
        },
        option: {
          column: [
            {
              label: '实心单选',
              prop: 'radio1',
              type: 'radio',
              span:24,
              button:true,
              dicData:DIC
            },{
              label: '空心单选',
              prop: 'radio2',
              type: 'radio',
              span:24,
              border:true,
              dicData:DIC
            },{
              label: '实心多选',
              prop: 'checkbox1',
              type: 'checkbox',
              span:24,
              button:true,
              dicData:DIC
            },{
              label: '空心多选',
              prop: 'checkbox2',
              span:24,
              type: 'checkbox',
              border:true,
              dicData:DIC
            }
          ]
        }
      }
    }
}
</script>

```
:::

