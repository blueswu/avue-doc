<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
const dic=[{
  value: 'zhinan',
  label: '指南',
  children: [{
    value: 'shejiyuanze',
    label: '设计原则',
    children: [{
      value: 'yizhi',
      label: '一致'
    }, {
      value: 'fankui',
      label: '反馈'
    }]
  }]
}]
export default {
    data() {
      return {
        form:{
          "cascader4": ["110000", "110100", "110101"]
        },
        option: {
          column: [{
            label: '多选',
            prop: 'cascader',
            type: "cascader",
            dicData: dic,
            multiple: true
          }, {
            label: '任意一级',
            prop: 'cascader1',
            type: "cascader",
            dicData: dic,
            checkStrictly: true
          }, {
            label: '搜索',
            prop: 'cascader2',
            type: "cascader",
            dicData: dic,
            filterable: true
          }, {
            label: '自定义',
            prop: 'cascader3',
            type: "cascader",
            dicData: dic,
            typeslot: true
          }, {
            label: '懒加载',
            prop: 'cascader4',
            type: "cascader",
            props: {
              label: 'name',
              value: 'code'
            },
            lazy: true,
            lazyLoad(node, resolve) {
              let stop_level = 2;
              let level = node.level;
              let data = node.data || {}
              let code = data.code;
              let list = [];
              let callback = () => {
                resolve((list || []).map(ele => {
                  return Object.assign(ele, {
                    leaf: level >= stop_level
                  })
                }));
              }
              if (level == 0) {
                axios.get(`${baseUrl}/getProvince`).then(res => {
                  list = res.data.data;
                  callback()
                })
              }
              if (level == 1) {
                axios.get(`${baseUrl}/getCity/${code}`).then(res => {
                  list = res.data.data;
                  callback()
                })
              } else if (level == 2) {
                axios.get(`${baseUrl}/getArea/${code}`).then(res => {
                  list = res.data.data;
                  callback()
                })
              }
            }
          }]
      }
    }
  }
}
</script>



## 表单级联选择器方法

:::tip
 2.6.6+
::::

:::demo 
```html
<avue-form v-model="form" :option="option">
  <template slot="cascader3Type" slot-scope="{node,data}">
    <span>{{ (data || {}).label }}</span>
    <span v-if="!node.isLeaf"> ({{((data || {}).children || []).length }}) </span>
  </template>
</avue-form>
<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
const dic=[{
  value: 'zhinan',
  label: '指南',
  children: [{
    value: 'shejiyuanze',
    label: '设计原则',
    children: [{
      value: 'yizhi',
      label: '一致'
    }, {
      value: 'fankui',
      label: '反馈'
    }]
  }]
}]
export default {
    data() {
      return {
        form:{
          "cascader4": ["110000", "110100", "110101"]
        },
        option: {
          column: [{
            label: '多选',
            prop: 'cascader',
            type: "cascader",
            dicData: dic,
            multiple: true
          }, {
            label: '任意一级',
            prop: 'cascader1',
            type: "cascader",
            dicData: dic,
            checkStrictly: true
          }, {
            label: '搜索',
            prop: 'cascader2',
            type: "cascader",
            dicData: dic,
            filterable: true
          }, {
            label: '自定义',
            prop: 'cascader3',
            type: "cascader",
            dicData: dic,
            typeslot: true
          }, {
            label: '懒加载',
            prop: 'cascader4',
            type: "cascader",
            props: {
              label: 'name',
              value: 'code'
            },
            lazy: true,
            lazyLoad(node, resolve) {
              let stop_level = 2;
              let level = node.level;
              let data = node.data || {}
              let code = data.code;
              let list = [];
              let callback = () => {
                resolve((list || []).map(ele => {
                  return Object.assign(ele, {
                    leaf: level >= stop_level
                  })
                }));
              }
              if (level == 0) {
                axios.get(`${baseUrl}/getProvince`).then(res => {
                  list = res.data.data;
                  callback()
                })
              }
              if (level == 1) {
                axios.get(`${baseUrl}/getCity/${code}`).then(res => {
                  list = res.data.data;
                  callback()
                })
              } else if (level == 2) {
                axios.get(`${baseUrl}/getArea/${code}`).then(res => {
                  list = res.data.data;
                  callback()
                })
              }
            }
          }]
      }
    }
  }
}
</script>

```
:::