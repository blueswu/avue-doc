<script>
export default{
  data() {
    return {
      form: {
        map: [ 113.10235504165291, 41.03624227495205, "内蒙古自治区乌兰察布市集宁区新体路街道顺达源广告传媒" ] 
      },
      option: {
          column: [
            {
              label: '坐标',
              prop: 'map',
              type: 'map',
            }]
        }
    }
  }
}
</script>

# 坐标选择器

```
<!-- 导入需要的包 -->
<script type="text/javascript" src='https://webapi.amap.com/maps?v=1.4.11&key=key&plugin=AMap.PlaceSearch'></script>
<script src="https://webapi.amap.com/ui/1.0/main.js?v=1.0.11"></script>
```

## 普通用法 
:::demo  
```html
<avue-form :option="option" v-model="form" ></avue-form>

<script>
export default{
  data() {
    return {
      form: {
        map: [ 113.10235504165291, 41.03624227495205, "内蒙古自治区乌兰察布市集宁区新体路街道顺达源广告传媒" ] 
      },
      option: {
          column: [
            {
              label: '坐标',
              prop: 'map',
              type: 'map',
            }]
        }
    }
  }
}
</script>

```
:::
