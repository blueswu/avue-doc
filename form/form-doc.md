## FORM文档

## Variables

|参数|说明|类型|可选值|默认值|
|-------------|-------------------------------------------------------------|--------|------|------|
|option|组件配置属性，详情见下面Option属性|Object|—|—|
|upload-before|图片上传前的回调,会暂停图片上传function(file,done,loading)，done用于继续图片上传，loading用于中断操作|Function|—|—|
|upload-after|图片上传后的回调,function(res,done)，done用于结束操作|Function|—|—|
|upload-delete|删除前的回调,返回true/false即可或promise对象function(column)|Function|—|—|
|upload-preview|查看前的回调function(file,column)|Function|—|—|



## Option Attributes

|参数|说明|类型|可选值|默认值|
|-------------|-------------------|-------|---------------------|------|
|card|卡片效果|Boolean|true/false|false|
|emptyBtn|清空按钮|Boolean|true/false|true|
|emptySize|清空按钮的大小|String|medium/mini/small|medium|
|emptyText|清空按钮的文字|String|-|清空|
|enter|回车提交表单|Boolean|true/false|true|
|group|分组|Array|-|-|
|gutter|项之间的间|Number|-|20|
|icon|表单的标题图标|String|-|80|
|label|表单的标题名称|String|-|80|
|labelWidth|表单的label宽度|String|-|80|
|labelPosition|表单的label位置|String|left/top/right|left|
|arrow|分组折叠|Boolean|true/false|true|
|collapse|分组默认叠起|Boolean|true/false|false|
|menuBtn|是否显示按钮|Boolean|true/false|true|
|menuSpan|菜单的span|Number|1-24|24|
|menuPosition|按钮的位置|String|left/center/right|center|
|prop|表单的标题列字段|String|-|80|
|props|字典的全局key配置|Object|-|-|
|size|表单全局控件的大小|String|medium/mini/small|medium|
|submitBtn|提交按钮|Boolean|true/false|true|
|submitSize|提交按钮的大小|String|medium/mini/small|medium|
|submitText|提交按钮的文字|String|-|提交|


## Props Attributes

|参数|说明|类型|可选值|默认值|
|--------|------------------|------|------|------|
|label|字典的名称属性值|String|—|—|
|value|字典的值属性值|String|—|—|
|children|字典的子属性值|String|—|—|
|res|网络字典返回的数据格式|String|—|—|


## Type Attributes

:::tip
这是type属性可以配置的组件，当然你也可以自定义，参考第三方组件导入[在线例子](/doc/form/form-component)
:::


|参数|类型|
|--------|------------------|
|array|数组框|
|color|颜色选择框|
|cascader|级联框|
|checkbox|多选框|
|date|日期框|
|datetime|日期时间框|
|daterange|日期范围|
|datetimerange|日期时间范围|
|dates|多个日期|
|dynamic|子表单|
|icon|图标选择框|
|input|输入框|
|img|图片框|
|month|月|
|password|密码框|
|radio|单选框|
|select|选择框|
|switch|开关框|
|slider|滑动框|
|textarea|文本框|
|rate|评价框|
|time|时间框|
|timerange|时间范围|
|tree|树框|
|url|超链|
|week|周|
|year|年|





## Format Attributes

使用`format`指定输入框的格式；使用`valueFormat`指定绑定值的格式。

默认情况下，组件接受并返回`Date`对象。以下为可用的格式化字串，以UTC2017年1月2日03:04:05为例：

:::warning
请注意大小写
:::

|格式|含义|备注|举例|
|-----------|---------|------------------------------------------------|-------------|
|`yyyy`|年||2017|
|`M`|月|不补0|1|
|`MM`|月||01|
|`W`|周|仅周选择器的`format`可用；不补0|1|
|`WW`|周|仅周选择器的`format`可用|01|
|`d`|日|不补0|2|
|`dd`|日||02|
|`H`|小时|24小时制；不补0|3|
|`HH`|小时|24小时制|03|
|`h`|小时|12小时制，须和`A`或`a`使用；不补0|3|
|`hh`|小时|12小时制，须和`A`或`a`使用|03|
|`m`|分钟|不补0|4|
|`mm`|分钟||04|
|`s`|秒|不补0|5|
|`ss`|秒||05|
|`A`|AM/PM|仅`format`可用，大写|AM|
|`a`|am/pm|仅`format`可用，小写|am|
|`timestamp`|JS时间戳|仅`value-format`可用；组件绑定值为`number`类型|1483326245000|

## Column Attributes

|参数|说明|类型|可选值|默认值|
|----------|-------------|-----------|--------------------|---------------------|
|all|是否启动全选(checkbox生效)|Boolean|true/false|false|
|accept|为upload时文件类型|String/Array|—|-|
|cascaderChange|级联选择时下级是否自选|Boolean|true/false|true|
|dataType|数据的类型转换也就是数组和用逗号隔开的字符串（适用于checkbox/select多选/tree多选/img/array/upload）|String|number/string|-|
|cascaderItem|级联的子节点prop|Array|-|-|
|cascaderIndex|级联的默认选项|Number|-|-|
|changeOnSelect|是否允许选择任意一级的选项|Boolean|true/false|false|
|clearable|表单清空|Boolean|true/false|false|
|disabled|全部是否禁止|Boolean|true/false|false|
|data|携带的附加参数(upload生效)|Object|-|-|
|headers|携带的头部附加参数(upload生效)|Object|-|-|
|cascaderIndex|级联选择下级默认选择的index|Number|-|1|
|endPlaceholder|日期范围结束占位符|String|-|-|
|formslot|表单自定义|Boolean|true/false|false|
|filesize|为upload时文件的大小|Number|-|-|
|filterable|级联框是否可以搜索|Boolean|true/false|false|
|Function(value,row,column)|-|-|
|format|显示值时间格式（当type为date/time/datetime/daterangetimerange/datetimerange/week/month/year/dates|-|-|-|
|formatter|用来格式化内容|Function(row,value,label,column)|-|-|
|label|列名称|String|—|-|
|typeslot|表单组件自定义|Boolean|true/false|false|
|multiple|多选（当type为select/tree时）|Boolean|true/false|false|
|minRows|最小行（当type为textarea）|Number|-|2|
|maxRows|最大行（当type为textarea）|Number|-|4|
|offset|当前项的偏移量|Number|-|0|
|labelSuffix|label后缀字符串|String|-|:|
|precision|数字框输入精度（当type为number时）|Number|-|2|
|prop|列字段|String|—|-|
|placeholder|辅助语|String|—|请选择/请输入+label|
|readonly|只读|Boolean|true/false|false|
|rules|表单规则,参考ele表单规则配置|Object|-|-|
|row|截断后面的排列|Boolean|true/false|false|
|span|表单栅列|Number|-|12|
|startPlaceholder|日期范围开始占位符|String|-|-|
|size|表单大小|String|small/mini|small|
|showSllLevels|级联输入框中是否显示选中值的完整路径|Boolean|true/false|true|
|separator|级联输入框选项分隔符|String|-|斜杠'/'|
|tip|辅助提示语|String|-|-|
|tipPlacement|辅助提示语的方向|String|top-start/top/top-end/left-start/leftleft-end/right-start/right/right-endbottom-start/bottom/bottom-end|bottom|
|type|类型|String|input/select/radio/checkbox/textarea/cascader/date/time/datetime/daterange/timerange/datetimerange/week/month/year/dates/ueditor/password/switch/tree|input|-|
|display|全部是否可见|Boolean|true/false|true|
|checkStrictly|不遵循父子规则|Boolean|true/false|false|
|tags|select组件多选时是否超出后合并标签|Boolean|true/false|false|
|valueFormat|真实值的时间格式（当type为date/time/datetime/daterangetimerange/datetimerange/week/month/year/dates）|-|-|-|
|value|默认值|-|—|-|



## Events

|事件名|说明|参数|
|------------|------------|----|
|reset-change|重置表单回调||
|submit|表单提交事件||

## Methods
|方法名|说明|参数|
|-------------|--------------------|----|
|init|初始化参数（比如服务端加载option，需要调用该内置函数初始化）|
|getPropRef|获取prop的ref对象|prop|
|clearValidate|清空表格钟表单验证。||
|validate|对整个表单进行校验的方法，参数为一个回调函数。该回调函数会在校验结束后被调用，并传入两个参数：是否校验成功和未通过校验的字段。若不传入回调函数，则会返回一个promise||
|resetForm|清空表单数据||
|submit|提交表单数据||

## ScopedSlot

|name|说明|
|----|---|
|列的名称|列自定义列的内容，参数为{row,label,dic,$index}|
|列表单的名称|表单自定义列的内容，参数为{row,label,dic,$index}|
|列表单的名称+Header(nameHeader)|表单分组头部自定义的内容|
|列表单的名称+Type(nameType)|表单自定义列的内容，参数为{row,label,value}|
|menuForm|表单操作按钮的自定义内容,参数为{row,label,dic,$index}|