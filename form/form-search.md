<script>
export default{
  data() {
    return {
      form:{},
      option: {
        labelWidth: 120,
        column: [
          {
            label: '搜索内容',
            prop: 'search',
            type: 'search',
            appendClick:()=>{
             this.$message.success('搜索回调'+this.form.search);
            }
          }
        ]
      }
    }
  }
}
</script>

# 搜索输入框
:::tip
 1.0.6+
::::


## 普通用法 
:::demo  配置`type`为`search`,`appendClick`为点击搜索的回调方法
```html
<avue-form :option="option" v-model="form" > </avue-form>

<script>
export default{
  data() {
    return {
      form:{},
      option: {
        labelWidth: 120,
        column: [
          {
            label: '搜索内容',
            prop: 'search',
            type: 'search',
            appendClick:()=>{
              this.$message.success('搜索回调'+this.form.search);
            }
          }
        ]
      }
    }
  }
}
</script>

```
:::
