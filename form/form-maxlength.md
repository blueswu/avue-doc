<script>
export default{
  data() {
    return {
      form: {
        input:'欢迎使用avue',
        textarea:'欢迎使用avue',
      },
      option: {
        column: [
         {
            label:'单文本框',
            prop:'input',
            maxlength:10,
            showWordLimit:true
         }, {
            label:'多文本框',
            prop:'textarea',
            type:'textarea',
            minRows:10,
            maxlength:200,
            span:24,
            showWordLimit:true
         }
        ]
      }
    }
  },
  methods: {
    submit() {
      this.$message.success('当前数据' + JSON.stringify(this.form))
    }
  }
}
</script>

# 字符长度提示

右下角和右侧的字符长度提示

:::tip
1.0.9+
::::


## 普通用法 
:::demo  配置`showWordLimit`为`true`即可
```html
<avue-form :option="option" v-model="form" > </avue-form>

<script>
export default{
  data() {
    return {
      form: {
        input:'欢迎使用avue',
        textarea:'欢迎使用avue',
      },
      option: {
        column: [
         {
            label:'单文本框',
            prop:'input',
            maxlength:10,
            showWordLimit:true
         }, {
            label:'多文本框',
            prop:'textarea',
            type:'textarea',
            minRows:10,
            maxlength:200,
            span:24,
            showWordLimit:true
         }
        ]
      }
    }
  },
  methods: {
    submit() {
      this.$message.success('当前数据' + JSON.stringify(this.form))
    }
  }
}
</script>

```
:::
