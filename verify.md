<script>
export default {
  data(){
    return {
      data:null,
      len:6,
    }
  },
  mounted(){
    setTimeout(()=>{
      this.$refs.video.startRecord();
      setTimeout(()=>{
        this.$refs.video.stopRecord();
      },5000)
    },3000)
  },
  methods:{
    dataChange(data) {
        function dataURLtoFile(dataurl, filename) {
          let arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);
          while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
          }
          return new File([u8arr], filename, {
            type: mime
          });
        }
        var client = new window.OSS({
          region: 'oss-cn-beijing',
          endpoint: 'oss-cn-beijing.aliyuncs.com',
          accessKeyId: 'DVVDfw5e3GK53rxt',
          accessKeySecret: 'T3xEVl1BPOOmBSEfz8w0Qpp9lqmyHa',
          bucket: 'avue'
        });
        var file=dataURLtoFile(data,new Date().getTime()+'.mp4');
        client.put(file.name, file);
    }
  }
}
</script>

# Verify 验证码
结合video组件来做一些活体认证，或则是其他方面的验证
:::tip
 2.1.0
::::

:::demo 
```html
    <el-button @click="$refs.verify.randomn()" type="primary">随机验证码</el-button>
    <br /><br />
    <span style="font-size: 24px;line-height: 24px;color: #333;">请使用普通话朗读下方验证码</span>
    <br /><br />
    <avue-verify v-model="data" :len="len" ref="verify"></avue-verify>
    <br /><br />
    <avue-video background="https://avuejs.com/images/face.png" @data-change="dataChange" ref="video"></avue-video>
<script>
export default {
  data(){
    return {
      data:null,
      len:6,
    }
  }
}
</script>

```
:::

