<script>
export default {
  
}
</script>

# 入场动画

```
<!-- 导入动画需要的包 -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" />
```


## 普通用法
:::demo  `enter`为入场动画的名称（参考animate官网 https://daneden.github.io/animate.css），`delay`为延迟时间
```html
<avue-queue>
  <div style="width:100px;height:100px;background-color:#00a680;margin-bottom:10px;"></div>
</avue-queue>
<avue-queue enter="fadeInRight">
  <div style="width:100px;height:100px;background-color:#00a680"></div>
</avue-queue>
<script>
export default {

}
</script>
```
:::


