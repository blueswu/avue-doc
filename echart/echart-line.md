<script>
echarts.registerTheme(theme.themeName, theme.theme)
echarts.registerTheme(theme1.themeName, theme1.theme)
echarts.registerTheme(theme2.themeName, theme2.theme)
export default {
data() {
    return {
      theme:'',
      data:{
        categories: [
          "苹果",
          "三星",
          "华为",
          "oppo",
          "vivo",
          "小米"
        ],
        series: [
          {
            name: "手机品牌",
            data: [
              1000879,
              678564,
              897744,
              439087,
              478900,
              390877
            ]
          }, {
            name: "其他手机品牌",
            data: [
              2000879,
              678564,
              897744,
              439087,
              478900,
              390877
            ]
          }
        ]
      },
      option: {
        switchTheme:true,
        width: 1200,
        height: 600,
        title: '手机大比拼',
        smooth: true,//是否顺滑
        areaStyle: true,//是否面积
      }
    }
  }
}
</script>

# EchartLine 折现图
:::tip
 1.1.0+
::::

```
<!-- 导入需要的包 -->  
<script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/4.2.1/echarts.min.js"></script>
<script src="https://avuejs.com/theme/avue.project.js"></script>
<script src="https://avuejs.com/theme/halloween.project.js"></script>
<script src="https://avuejs.com/theme/wonderland.project.js"></script>
```

:::demo 
```html
<el-button type="primary" @click="option.switchTheme=!option.switchTheme" size="small">{{option.switchTheme?'关闭主题':'打开主题'}}</el-button>
<el-button @click="theme='macarons'" size="small">换紫色主题</el-button>
<el-button @click="theme='wonderland'" size="small">换绿色主题</el-button>
<avue-echart-line :theme="theme" :option="option" :data="data" width="1000"></avue-echart-line>
<script>
echarts.registerTheme(theme.themeName, theme.theme)
echarts.registerTheme(theme1.themeName, theme1.theme)
echarts.registerTheme(theme2.themeName, theme2.theme)
export default {
data() {
    return {
      theme:'',
      data:{
        categories: [
          "苹果",
          "三星",
          "华为",
          "oppo",
          "vivo",
          "小米"
        ],
        series: [
          {
            name: "手机品牌",
            data: [
              1000879,
              678564,
              897744,
              439087,
              478900,
              390877
            ]
          }, {
            name: "其他手机品牌",
            data: [
              2000879,
              678564,
              897744,
              439087,
              478900,
              390877
            ]
          }
        ]
      },
      option: {
        switchTheme:true,
        width: 1200,
        height: 600,
        title: '手机大比拼',
        smooth: true,//是否顺滑
        areaStyle: true,//是否面积
      }
    }
  }
}
</script>

```
:::




