## 全局配置

在引入 Avuex 时，可以传入一个全局配置对象。该对象目前支持 `size`和`menuType`字段。`size`用于改变组件的默认尺寸;`menuType`用于改变操作栏菜单按钮类型，按照引入 Avue 的方式，具体操作如下：

- 拥有 `theme` 主题颜色配置，属性的组件的默认白色。可选值 dark;
- 拥有 `size` 属性的组件的默认尺寸均为 'small'。可选值 small / mini / medium;
- 拥有 `menuType` 属性的组件的默认尺寸均为 'menu'。可选值 button / icon / text / menu
- 拥有 `qiniu` 七牛云配置
```
  {
    AK: '',
    SK: '',
    scope: '',
    url: '',
    deadline: 1
  }
```
- 拥有 `ali` 阿里云配置
```
  {
    region: '',
    endpoint: '',
    accessKeyId: '',
    accessKeySecret: '',
    bucket: '',
  }
```
- 拥有 `canvas`全局水印配置
```
  {
    text: 'avuejs.com',
    fontFamily: 'microsoft yahei',
    color: "#999",
    fontSize: 16,
    opacity: 100,
    bottom: 10,
    right: 10,
    ratio: 1
  }
```

```
Vue.use(window.AVUE,{
  size:'',
  menuType:'',
  qiniu:{},
  ali:{},
  theme:'',//dark黑色主题
  canvas:{}
})
```