<script>
export default {
 data() {
    return {
      option1:{
        expand: true,
        expandRowKeys:[],
        rowKey:'id',
        column: [{
          label: '姓名',
          prop: 'name'
        }, {
          label: '年龄',
          prop: 'sex'
        }]
      },
      option: {
        expand: true,
        rowKey:'id',
        expandRowKeys:[1],
        column: [{
          label: '姓名',
          prop: 'name'
        }, {
          label: '年龄',
          prop: 'sex'
        }]
      },
      data: [{
        id: 1,
        name: '张三',
        sex: 12,
      }, {
        id: 2,
        name: '李四',
        sex: 20,
      }]
    }
  },
   methods: {
    expandChange(row, expendList) {
      this.$message.success('展开回调')
    },
     expandChanges(row, expendList) {
      if (expendList.length) {
        this.option1.expandRowKeys = []
        if (row) {
          this.option1.expandRowKeys.push(row.id)
        }
      } else {
        this.option1.expandRowKeys = []
      }
      this.$message.success('展开回调')
    }
  }
}
</script>

# 表格行展开
指定行展开，或则默认全部展开
:::tip
 1.0.8+
::::


## 普通用法
:::demo  使用`expand`属性时必须配置`rowKey`属性为你行数据的主键，不能重复, `defaultExpandAll`属性默认展开全部,`expandRowKeys`为展开指定`rowKey`主键的数组，同时你也可以调用`toggleRowExpansion`方法传入你要展开的`row`
```html
<avue-crud ref="crud" :option="option" :data="data" @expand-change="expandChange">
  <template slot="expand" slot-scope="{row}">
    {{row}}
  </template>
</avue-crud>
<script>
export default {
 data() {
    return {
      option: {
        expand: true,
        expandRowKeys:[1],
        rowKey:'id',
        column: [{
          label: '姓名',
          prop: 'name'
        }, {
          label: '年龄',
          prop: 'sex'
        }]
      },
      data: [{
        id: 1,
        name: '张三',
        sex: 12,
      }, {
        id: 2,
        name: '李四',
        sex: 20,
      }]
    }
  }, methods: {
    expandChange(row, expendList) {
      this.$message.success('展开回调')
    },
  }
}
</script>

```
:::

## 手风琴模式
:::demo  `expand-change`配置`expandRowKeys`去使用
```html
<avue-crud ref="crud" :option="option1" :data="data" @expand-change="expandChanges">
  <template slot="expand" slot-scope="{row}">
    {{row}}
  </template>
</avue-crud>
<script>
export default {
 data() {
    return {
      option1:{
        expand: true,
        expandRowKeys:[],
        rowKey:'id',
        column: [{
          label: '姓名',
          prop: 'name'
        }, {
          label: '年龄',
          prop: 'sex'
        }]
      },
      data: [{
        id: 1,
        name: '张三',
        sex: 12,
      }, {
        id: 2,
        name: '李四',
        sex: 20,
      }]
    }
  }, 
  methods: {
   expandChanges(row, expendList) {
      if (expendList.length) {
        this.option1.expandRowKeys = []
        if (row) {
          this.option1.expandRowKeys.push(row.id)
        }
      } else {
        this.option1.expandRowKeys = []
      }
      this.$message.success('展开回调')
    }
  }
}
</script>

```
:::


## Attributes
|参数|说明|类型|可选值|默认值|
|----------------|------------------------------------------------------------------------------------------------------------------|---------------|---------------------------|--------|
|defaultExpandAll|是否默认展开所有行，"expand"为true的时候有效|Boolean|true/false|false|
|expandRowKeys|可以通过该属性设置目前的展开行，需要设置 rowKey 属性才能使用，该属性为展开行的 keys 数组。|Array|-|-|
|rowKey|行数据的 Key的主键，用于其他相关操作|String|—|id|

## Events

|事件名|说明|参数|
|------------------|---------------------------|-------------------------|
|toggleRowExpansion|用于可展开表格，切换某一行的展开状态，如果使用了第二个参数，则是设置这一行展开与否（expanded 为 true 则展开|row, expanded|


