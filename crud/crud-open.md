<script>
  export default {
    data() {
      return {
        form:{},
        data: [{
            name:'张三',
            sex:'男'
        }],
        option:{
          align:'center',
          menuAlign:'center',
          viewBtn:true,
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    },
    methods: {
      beforeOpen(done,type){
        this.$alert(`我是${type}`, {
          confirmButtonText: '确定',
          callback: action => {
            if(['views','edit'].includes(type)){
            // 查看和编辑逻辑
            }else{
              //新增逻辑
              //一定要用setTimeout包裹，由于form组件底层一些机制的设计
              setTimeout(()=>{
                  this.form.name='初始化赋值'
              },0)
            }
            done();
          }
        });
          
      }
    }
  };
</script>


# 弹窗打开前
:::tip
 1.0.0+
::::




:::demo 其实在这个方法里面可以干很多操作，看例子代码

```html
<avue-crud
  :data="data"
  v-model="form"
  :before-open="beforeOpen"
  :option="option"
></avue-crud>

<script>
  export default {
    data() {
      return {
        form:{},
        data: [{
            name:'张三',
            sex:'男'
        }],
        option:{
          align:'center',
          menuAlign:'center',
          viewBtn:true,
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    },
    methods: {
      beforeOpen(done,type){
        this.$alert(`我是${type}`, {
          confirmButtonText: '确定',
          callback: action => {
            if(['views','edit'].includes(type)){
            // 查看和编辑逻辑
            }else{
              //新增逻辑
              //一定要用setTimeout包裹，由于form组件底层一些机制的设计
              setTimeout(()=>{
                  this.form.name='初始化赋值'
              },0)
            }
            done();
          }
        });
          
      }
    }
  };
</script>
```
