<script>
export default {
    data() {
      return {
        data: [
          {
            id:1,
            name:'张三',
            sex:'男'
          }, {
            id:2,
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          index:true,
          indexFixed:false,
          indexWidth:100,
          selection:true,
          selectionWidth:100,
          selectionFixed:false,
          expand:true,
          expandWidth:100,
          expandFixed:false,
          align:'center',
          menuAlign:'center',
          column:[
             {
              width:500,
              label:'姓名',
              prop:'name'
            }, {
              width:500,
              label:'性别',
              prop:'sex'
            }
          ]
        }
      }
    }
}
</script>



# 冻结列
:::tip
 2.0.4+
::::


## 普通用法

:::demo 配置`indexFixed`,`selectionFixed`,`expandFixed`可以配置序号，多选，面板是否为冻结,当然你也可以配置他们的宽度`indexWdth`,`selectionWidth`,`expandWidth`。
```html
<avue-crud :data="data" :option="option" ></avue-crud>

<script>
export default {
    data() {
      return {
        data: [
          {
            id:1,
            name:'张三',
            sex:'男'
          }, {
            id:2,
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          index:true,
          indexFixed:false,
          indexWidth:100,
          selection:true,
          selectionWidth:100,
          selectionFixed:false,
          expand:true,
          expandWidth:100,
          expandFixed:false,
          align:'center',
          menuAlign:'center',
          column:[
             {
              width:500,
              label:'姓名',
              prop:'name'
            }, {
              width:500,
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    }
}
</script>

```
:::


