<script>
export default {
  data(){
    return {
      search:{},
      data0:[
         {
          text1:'文本1',
          text2:'文本2'
         }
       ],
       option0:{
          column: [{
            label: '内容1',
            prop: 'text1',
            search:true,
            searchTip:'我是一个默认提示语',
          },{
            label: '内容2',
            prop: 'text2',
            search:true,
            searchTip:'我是一个左边提示语',
            searchTipPlacement:'left',
          }]
       },
      option1:{
          searchMenuSpan:8,
          column: [{
            label: '内容1',
            prop: 'text1',
          },{
            label: '内容2',
            prop: 'text2',
            search:true,
          }]
       },
       data:[{
         name:'张三'
       }],
       option:{
          column: [{
            label: '姓名',
            prop: 'name',
            searchValue:'small',
            search:true,
            searchRules: [{
              required: true,
              message: "请输入姓名",
              trigger: "blur"
            }],
          },{
            label: '日期',
            prop: 'date',
            type:'datetime',
            searchSpan:12,
            searchRange:true,
            search:true,
          }]
       }
    }
  },
  methods:{
      searchSubmit(item){
        this.$message.success(JSON.stringify(item))
      },
      searchChange(params,done) {
        done();
        this.$message.success('search callback' + JSON.stringify(Object.assign(params, this.searchForm)))
      },
  }
}
</script>

# 搜索


:::tip
 2.1.0+
::::

## 普通用法
:::demo  `searchSpan`搜索框的宽度，`searchRange`配置后可以开启范围搜索（date,datetime,time）,当然你可能首次打开不想显示搜索可以配置`searchShow`为`false`,点击搜索小图标即可展开,`searchValue`为搜索的默认值
```html
{{search}}
<avue-crud :option="option" :search.sync="search" :data="data" @search-change="searchChange">
  <template slot="searchMenu"  slot-scope="{row,size}">
      <el-button :size="size" @click="searchSubmit(row)">自定义提交</el-button>
  </template>
  <template slot="search" slot-scope="{row,size}">
    <el-tag>这里是自定义内容</el-tag>
  </template>
</avue-crud>
<script>
export default {
  data(){
    return {
       search:{},
       data:[{
         name:'张三'
       }],
       option:{
          column: [{
            label: '姓名',
            prop: 'name',
            searchValue:'small',
            search:true,
            searchRules: [{
              required: true,
              message: "请输入姓名",
              trigger: "blur"
            }],
          },{
            label: '日期',
            prop: 'date',
            type:'datetime',
            searchSpan:12,
            searchRange:true,
            search:true,
          }]
       }
    }
  },
  methods:{
      searchSubmit(item){
        this.$message.success(JSON.stringify(item))
      },
      searchChange(params,done) {
        done();
        this.$message.success(JSON.stringify(params));
      }
  }
}
</script>

```
:::



## 辅助提示语
:::demo  前提的`column`中要启动搜索的字典`search`设置为`true`,`searchTip`为提示的内容,`searchTipPlacement`为提示语的方向，默认为`bottom`
```html
<avue-crud :option="option0" :data="data0" @search-change="searchChange"></avue-crud>
<script>
export default {
  data(){
    return {
       data0:[
         {
          text1:'文本1',
          text2:'文本2'
         }
       ],
       option0:{
          column: [{
            label: '内容1',
            prop: 'text1',
            search:true,
            searchTip:'我是一个默认提示语',
          },{
            label: '内容2',
            prop: 'text2',
            search:true,
            searchTip:'我是一个左边提示语',
            searchTipPlacement:'left',
          }]
       }
    }
  },
  methods:{
      searchChange(params,done) {
        done();
        this.$message.success(JSON.stringify(params));
      }
  }
}
</script>

```
:::


## 按钮是否单独成行
:::demo  前提的`searchMenuSpan`可以控制搜索按钮的长度
```html
<avue-crud :option="option1" :data="data0" @search-change="searchChange"></avue-crud>
<script>
export default {
  data(){
    return {
       data0:[
         {
          text1:'文本1',
          text2:'文本2'
         }
       ],
       option1:{
          searchMenuSpan:8,
          column: [{
            label: '内容1',
            prop: 'text1',
            search:true,
          },{
            label: '内容2',
            prop: 'text2',
            search:true,
          }]
       }
    }
  },
  methods:{
      searchChange(params,done) {
        done();
        this.$message.success(JSON.stringify(params));
      }
  }
}
</script>

```
:::
