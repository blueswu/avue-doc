<script>
export default {
    data() {
      return {
        option: {
          column: [{
            label: '姓名',
            prop: 'name',
            renderHeader: (h, { column, $index }) => {
              return h('div',[
                h('el-tag', column.label),
                h('spam', '自定义表头'),
              ])
            },
          }, {
            label: '年龄',
            prop: 'sex'
          }]
        },
        data: []
      }
    }
}
</script>

# 自定义表头
:::tip
 1.0.8+
::::

## 普通用法
:::demo 调用`renderHeader`函数即可自定义表头，具体操作看如下例子
```html

<avue-crud ref="crud" :option="option" :data="data"></avue-crud>
<script>
export default {
    data() {
      return {
        option: {
          column: [{
            label: '姓名',
            prop: 'name',
            renderHeader: (h, { column, $index }) => {
              return h('div',[
                h('el-tag', column.label),
                h('spam', '自定义表头'),
              ])
            },
          }, {
            label: '年龄',
            prop: 'sex'
          }]
        },
        data: []
      }
    }
}
</script>

```
:::


## Variables
|参数|说明|类型|可选值|默认值|
|----------------------|--------------------------------|--------|------------|------|
renderHeader|列标题 Label 区域渲染使用的|Function|Function(h, { column, $index })|—|—|

