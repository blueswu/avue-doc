<script>
export default {
    data() {
      return {
        box:false,
        data: [
          {
            name:'张三',
            sex:true
          }, {
            name:'李四',
            sex:false
          }
        ],
        option:{
          dialogDrag:true,
          column:[
             {
              label:'姓名',
              prop:'name',
            }, {
              label:'性别',
              prop:'sex',
              type:'switch',
            }
          ]
        }
      }
    }
}
</script>



# 表单拖拽
- 点击新增或则编辑即可拖动表单
- 同时已经将拖拽封装成指令，在el的原声dialog上添加v-dialogdrag即可拖拽
:::tip
 2.5.3+
::::


## 普通用法

:::demo `dialogDrag`设置为`true`即可拖动表单
```html
<avue-crud :data="data" :option="option">
  <template slot="menuLeft" slot-scope="{}" >
    <el-button icon="el-icon-eleme" size="small" @click="box=true">原生弹窗</el-button>
  </template>
</avue-crud>
<el-dialog
  title="提示"
  :visible.sync="box"
  class="avue-dialog"
  v-dialogdrag
  width="40%" >
  <span>这是一段信息</span>
  <span slot="footer" class="dialog-footer">
    <el-button @click="box=false">取 消</el-button>
    <el-button type="primary" @click="box=false" >确 定</el-button>
  </span>
</el-dialog>
<script>
export default {
    data() {
      return {
        box:false,
        data: [
          {
            name:'张三',
            sex:true
          }, {
            name:'李四',
            sex:false
          }
        ],
        option:{
          dialogDrag:true,
          column:[
             {
              label:'姓名',
              prop:'name',
            }, {
              label:'性别',
              prop:'sex',
              type:'switch',
            }
          ]
        }
      }
    }
}
</script>


```
:::


