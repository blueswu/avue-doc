<script>
export default {
  data(){
    return {
      option:{
        lazy:true,
        rowKey:'id',
        column:[{
          label:'ID',
          prop:'id'
        },{
          label:'姓名',
          prop:'name'
        },{
          label:'日期',
          prop:'date'
        },{
          label:'地址',
          prop:'address',
          overHidden:true
        }]
      },
       data: [{
          id: 1,
          date: '2016-05-02',
          name: '王小虎',
          address: '上海市普陀区金沙江路 1518 弄'
        }, {
          id: 2,
          date: '2016-05-04',
          name: '王小虎',
          address: '上海市普陀区金沙江路 1517 弄'
        }, {
          id: 3,
          date: '2016-05-01',
          name: '王小虎',
          address: '上海市普陀区金沙江路 1519 弄',
          hasChildren: true
        }, {
          id: 4,
          date: '2016-05-03',
          name: '王小虎',
          address: '上海市普陀区金沙江路 1516 弄'
        }]
    }
  },
  methods:{
   treeLoad(tree, treeNode, resolve) {
        setTimeout(() => {
          resolve([{
              id: new Date().getTime(),
              date: '2016-05-01',
              name: '王小虎',
              address: '上海市普陀区金沙江路 1519 弄',
              hasChildren: true
            }
          ])
        }, 1000)
      }
  }
}
</script>

# 表格树懒加载
配置rowKey主键属性(默认为id),并且使用ele2.8.2+以上版本

:::tip
 2.3.0+
::::
## 普通用法
:::demo `lazy`为`true`,同时接受`tree-load`函数的回调即可，通过指定 row 中的 hasChildren 字段来指定哪些行是包含子节点
```html
<avue-crud :option="option" :data="data" @tree-load="treeLoad"></avue-crud>
<script>
export default {
  data(){
    return {
      option:{
        lazy:true,
        rowKey:'id',
        column:[{
          label:'ID',
          prop:'id'
        },{
          label:'姓名',
          prop:'name'
        },{
          label:'日期',
          prop:'date'
        },{
          label:'地址',
          prop:'address',
          overHidden:true
        }]
      },
       data: [{
          id: 1,
          date: '2016-05-02',
          name: '王小虎',
          address: '上海市普陀区金沙江路 1518 弄'
        }, {
          id: 2,
          date: '2016-05-04',
          name: '王小虎',
          address: '上海市普陀区金沙江路 1517 弄'
        }, {
          id: 3,
          date: '2016-05-01',
          name: '王小虎',
          address: '上海市普陀区金沙江路 1519 弄',
          hasChildren: true
        }, {
          id: 4,
          date: '2016-05-03',
          name: '王小虎',
          address: '上海市普陀区金沙江路 1516 弄'
        }]
    }
  },
  methods:{
   treeLoad(tree, treeNode, resolve) {
        setTimeout(() => {
          resolve([
            {
              id: new Date().getTime(),
              date: '2016-05-01',
              name: '王小虎',
              address: '上海市普陀区金沙江路 1519 弄'
            }, {
              id: new Date().getTime(),
              date: '2016-05-01',
              name: '王小虎',
              address: '上海市普陀区金沙江路 1519 弄',
              hasChildren: true
            }
          ])
        }, 1000)
      }
  }
}
</script>


```
:::


