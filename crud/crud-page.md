<script>
  export default {
    data() {
      return {
        page1: {
          currentPage: 1,
          total: 0,
          layout: "total,pager,prev, next",
          background:false,
          pageSize: 10
        },
        page: {
           pageSize: 20,
           pagerCount:5
        },
        data: [],
        option:{
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    },
    created(){
      this.getList()
    },
    methods: {
      getList(){
        this.page1.total=40;
        if(this.page1.currentPage===1){
          this.data=[{
            name:'张三',
            sex:'男'
          }]
        }else if(this.page1.currentPage==2){
          this.data=[{
            name:'李四',
            sex:'女'
          }]
        }
      },
      onLoad(page){
        this.$message.success('分页信息:'+JSON.stringify(page));
        this.page.total=40;
        //模拟分页
        if (this.page.currentPage === 1) {
          this.data = [
            {
              id:1,
              name: '张三',
              sex: '男'
            },{
              id:2,
              name: '李四',
              sex: '女'
            }
          ]
        } else if (this.page.currentPage == 2) {
          this.data = [
            {
              id:3,
              name: '王五',
              sex: '女'
            },{
              id:4,
              name: '赵六',
              sex: '女'
            }
          ]
        }
      },
      sizeChange(val){
        this.page1.currentPage=1;
        this.page1.pageSize=val;
        this.getList();
        this.$message.success('行数'+ val);
      },
      currentChange(val){
        this.page1.currentPage=val;
        this.getList();
        this.$message.success('页码'+val);
      },
    }
  };
</script>


# 分页
:::tip
 2.6.3+
::::

## 普通用法


:::demo 首次加载调用`on-load`方法加载数据，返回`page`分页对象信息,赋值`page`的`total`总条数即可,如果`total`为0的话，或者`simplePage`为true只有1页的时候，分页选择器会隐藏

```html
{{page}}
<avue-crud
  :data="data"
  :option="option"
  :page.sync="page"
  @on-load="onLoad"
></avue-crud>

<script>
  export default {
    data() {
      return {
        page: {
          pageSize: 20,
          pagerCount:5
        },
        data: [],
        option: {
          align: 'center',
          menuAlign: 'center',
          column: [
            {
              label: '姓名',
              prop: 'name'
            },
            {
              label: '性别',
              prop: 'sex'
            }
          ]
        }
      }
    },
    methods: {
      onLoad(page) {
        this.$message.success('分页信息:' + JSON.stringify(page))
        this.page.total = 40
        //模拟分页
        if (this.page.currentPage === 1) {
          this.data = [
            {
              id:1,
              name: '张三',
              sex: '男'
            },{
              id:2,
              name: '李四',
              sex: '女'
            }
          ]
        } else if (this.page.currentPage == 2) {
          this.data = [
            {
              id:3,
              name: '王五',
              sex: '女'
            },{
              id:4,
              name: '赵六',
              sex: '女'
            }
          ]
        }
      }
    }
  }
</script>
```

:::

## 自定义分页

:::demo 实际的用法要后台配置，将后台返回值赋值给 page 中的属性， `page`就是分页对象注入，page 对象中的`total`为总页数，`pageSizes`为分页信息，`currentPage`为当前第几页，`pageSize`每一页加载多少条数据，点击页码会调用`current-change`方法回调当前页数，返回当前第几页，点击每页多少条会调`size-change`方法回调

```html
{{page1}}
<avue-crud
  :data="data"
  :option="option"
  :page.sync="page1"
  @size-change="sizeChange"
  @current-change="currentChange"
></avue-crud>

<script>
  export default {
    data() {
      return {
        page1: {
          currentPage: 1,
          total: 0,
          layout: "total,pager,prev, next",
          background:false,
          pageSize: 10
        },
        data: [],
        option: {
          align: 'center',
          menuAlign: 'center',
          column: [
            {
              label: '姓名',
              prop: 'name'
            },
            {
              label: '性别',
              prop: 'sex'
            }
          ]
        }
      }
    },
    created() {
      this.getList()
    },
    methods: {
      getList() {
        this.page1.total = 40
        if (this.page1.currentPage === 1) {
          this.data = [
            {
              id:1,
              name: '张三',
              sex: '男'
            },{
              id:2,
              name: '李四',
              sex: '女'
            }
          ]
        } else if (this.page1.currentPage == 2) {
          this.data = [
            {
              id:3,
              name: '王五',
              sex: '女'
            },{
              id:4,
              name: '赵六',
              sex: '女'
            }
          ]
        }if (this.page1.currentPage === 1) {
          this.data = [
            {
              id:1,
              name: '张三',
              sex: '男'
            },{
              id:2,
              name: '李四',
              sex: '女'
            }
          ]
        } else if (this.page1.currentPage == 2) {
          this.data = [
            {
              id:3,
              name: '王五',
              sex: '女'
            },{
              id:4,
              name: '赵六',
              sex: '女'
            }
          ]
        }
      },
      sizeChange(val) {
        this.page1.currentPage = 1
        this.page1.pageSize = val
        this.getList()
        this.$message.success('行数' + val)
      },
      currentChange(val) {
        this.page1.currentPage = val
        this.getList()
        this.$message.success('页码' + val)
      }
    }
  }
</script>
```

:::

## page Attributes

|参数|说明|类型|可选值|默认值|
|--------|------------------|------|------|------|
|background|是否为分页按钮添加背景色|Boolean|—|true|
|total|总条目数|Number|—|0|
|currentPage|当前页数|Number|—|1|
|pageSize|每页显示条目个数|Number	|—|20|
|pageSizes|每页显示个数选择器的选项设置|Array|—|[10, 20, 30, 40, 50, 100]|
|pagerCount|页码按钮的数量，当总页数超过该值时会折叠|Number|—|7|
|layout|组件布局，子组件名用逗号分隔|String|—|sizes, prev, pager, next, jumper,total |


## page Event

|事件名称|说明|回调参数
|----|--------|------|
|size-change|	pageSize 改变时会触发|pageSize,{page,search}|
|current-change|currentPage 改变时会触发|currentPage,{page,search}|
|prev-click|用户点击上一页按钮改变当前页后触发|currentPage,{page,search}|
|next-click|用户点击下一页按钮改变当前页后触发|currentPage,{page,search}|