<script>
export default {
  data(){
    return {
       data:[{
         name:'张三',
         sex:18,
       }],
       option:{
          column: [{
            label: '姓名',
            prop: 'name',
            search:true,
          },{
            label: '年龄',
            prop: 'sex',
            searchslot:true,
            search:true,
          }]
       }
    }
  },
  methods:{
      searchChange(params,done) {
        done();
        this.$message.success(JSON.stringify(params))
      },
  }
}
</script>

# 自定义搜索组件
由于使用了form组件作为底层,要在search-change中调用done函数

:::tip
 2.2.3+
::::

## 搜索自定义
:::demo  配置`searchslot`为`true`即可开启自定义，卡槽的名字为`prop`+Search，`searchMenu`为按钮的卡槽
```html
<avue-crud :option="option" :data="data" @search-change="searchChange">
  <template slot-scope="scope" slot="sexSearch">
        <el-tag>自定义搜索内容</el-tag>
  </template>
  <template slot="searchMenu">
    <el-button size="small">自定义按钮</el-button>
  </template>
</avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
         name:'张三',
         sex:18,
       }],
       option:{
          column: [{
            label: '姓名',
            prop: 'name',
            search:true,
          },{
            label: '年龄',
            prop: 'sex',
            searchslot:true,
            search:true,
          }]
       }
    }
  },
  methods:{
      searchChange(params,done) {
        done();
        this.$message.success(JSON.stringify(params))
      },
  }
}
</script>

```
:::



