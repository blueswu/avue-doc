<script>
export default {
  data(){
    return {
       data:[{
          text1:'内容1-1',
          text2:'内容1-2'
       },{
          text1:'内容2-1',
          text2:'内容2-2'
       }],
       option:{
          align:'center',
          printBtn:true,
          addBtn:false,
          menu:false,
          column: [{
            label: '列内容1',
            prop: 'text1',
          }, {
            label: '列内容2',
            prop: 'text2',
          }]
       }
    }
  }
}
</script>

# 表格打印

常用的表格打印功能

## 普通用法
:::demo  `printBtn`设置为`true`即可开启打印功能，如果配置了`selection`属性，则需要勾选打印的数据，否则为全部
```html
<avue-crud :option="option" :data="data"></avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
          text1:'内容1-1',
          text2:'内容1-2'
       },{
          text1:'内容2-1',
          text2:'内容2-2'
       }],
       option:{
          align:'center',
          printBtn:true,
          addBtn:false,
          menu:false,
          column: [{
            label: '列内容1',
            prop: 'text1',
          }, {
            label: '列内容2',
            prop: 'text2',
          }]
       }
    }
  }
}
</script>

```
:::


