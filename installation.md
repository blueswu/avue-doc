# 快速上手

## 导入
:::tip
min是生产包，另一个则是开发包
::::
  - avue.js
  - avue.min.js

### cdn方式
> 将下载的包放入public目录下新建的lib下
```

//在index.html引入avuex的包
avue.min.js为压缩混淆包
avue.js为没有压缩混淆的包
<link rel="stylesheet" href="/lib/index.css" />
<script src="/lib/avue.min.js"></script>

//在main.js中使用
Vue.use(window.AVUE);
```

### npm方式
> 推荐使用 npm 的方式安装，它能更好地和 webpack 打包工具配合使用。
```
npm i @smallwei/avue -S

import Avue from '@smallwei/avue';
import '@smallwei/avue/lib/index.css';
Vue.use(Avue);
```
## 老项目升级
1.删除packages.json中的低版本  
2.重新执行npm i @smallwei/avue -S安装  
3.删除main.js中的旧版本引用，换成如下  
```
import Avue from '@smallwei/avue';
import '@smallwei/avue/lib/index.css';
Vue.use(Avue);
```
4.一些属性和组件的变更[变更说明](/doc/explain)  







