<script>
export default {
    data() {
      return {
        form:'rgba(255, 120, 0, 1)',
      }
    }
}
</script>



## inputColor颜色选择器

:::demo 
```html
<el-row :span="24">
  <el-col :span="6">
   值:{{form}}<br/>
   <avue-input-color  placeholder="请选择颜色" v-model="form" ></avue-input-color>
  </el-col>
</el-row>
<script>
export default {
    data() {
      return {
        form:'rgba(255, 120, 0, 1)'
      }
    }
}
</script>

```
:::