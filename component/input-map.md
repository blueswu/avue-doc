<script>
export default {
    data() {
      return {
        form:[ 113.10235504165291, 41.03624227495205, "内蒙古自治区乌兰察布市集宁区新体路街道顺达源广告传媒" ] ,
      }
    }
}
</script>



## inputMap地图选择器

:::demo 
```html
<el-row :span="24">
  <el-col :span="6">
   值:{{form}}<br/>
   <avue-input-map  placeholder="请选择地图" v-model="form" ></avue-input-map>
  </el-col>
</el-row>
<script>
export default {
    data() {
      return {
        form:[ 113.10235504165291, 41.03624227495205, "内蒙古自治区乌兰察布市集宁区新体路街道顺达源广告传媒" ] ,
      }
    }
}
</script>

```
:::