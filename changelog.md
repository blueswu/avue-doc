# 更新日志
:::tip
每次更新版本时候，记得看更新日志，小心踩坑～。～
::::
### 成为avue的贡献者，快来一起完善文档吧[文档源码](https://gitee.com/smallweigit/avue-doc)

## 2.6.9
### 2020-07-02
### 更新
- <font color="red">新增内部字符串和数组的转化，不需要配置dataType属性</font>
- <font color="red">新增inputMap地图组件[在线例子1](/doc/component/input-map),[在线例子2](/doc/form/form-input-map)
</font>
- 重构array和img组件[在线例子](/doc/crud/crud-url)
- 重构icon和color组件[在线例子](/doc/crud/crud-url)
- 优化color组件变更为inputColor组件[在线例子](/doc/component/input-color)
- 优化icon-select组件变更为inputIcon组件[在线例子](/doc/component/input-icon)
- 优化inputTable表格组件[在线例子1](/doc/component/input-table),[在线例子2](/doc/form/form-input-table)
- 新增全局水印方法的删除事件[在线例子](/doc/watermark)
- 修复crud组件beforeClose方法done失效问题[在线例子](/doc/crud/crud-close),[gitee_I1M634](https://gitee.com/smallweigit/avue/issues/I1M634)
- 修复dynamic组件隐藏时提交的空问题[gitee_I1MCIZ](https://gitee.com/smallweigit/avue/issues/I1MCIZ)
- 修复crud组件字典动态赋值的问题[在线例子](/doc/form/form-update-dic),[gitee_I1MG1Q](https://gitee.com/smallweigit/avue/issues/I1MG1Q)
- 修复select开启远程字典的重复问题[在线例子](/doc/form/form-select-remote),[gitee_I1MBBL](https://gitee.com/smallweigit/avue/issues/I1MBBL)


## 2.6.8
### 2020-06-29
### 更新
- <font color="red">修复核心字典的重复和赋值问题</font>
- <font color="red">新增input-table组件搜索用法[在线例子](/doc/form/form-input-table)</font>
- 优化crud组件refreshChange方法参数问题[gitee_I1LWLF](https://gitee.com/smallweigit/avue/issues/I1LWLF)
- 修复crud组件搜索消失的问题[gitee_I1LWG1](https://gitee.com/smallweigit/avue/issues/I1LWG1)
- 修复crud组件dicFlag参数重新加载字典[在线例子](/doc/crud/crud-dic)
- 修复array组件的赋值问题[在线例子](/doc/crud/crud-url)

## 2.6.7
### 2020-06-28
### 更新
- <font color="red">新增input-table组件[在线例子](/doc/form/form-input-table)</font>
- 新增crud组件的editDetail和addDetail方法[在线例子](/doc/crud/crud-ea)
- 修复crud组件搜索字典重复问题[gitee_I1LO5B](https://gitee.com/smallweigit/avue/issues/I1LO5B)
- 修复form组件清空时空值的问题[gitee_I1LT5U](https://gitee.com/smallweigit/avue/issues/I1LT5U)
- 修复tree组件返回node节点数据[在线例子](/doc/tree),[gitee_I1LLLJ](https://gitee.com/smallweigit/avue/issues/I1LLLJ)

## 2.6.6
### 2020-06-23
### 更新
- 优化checkbox组件的all默认为false[在线例子](/doc/component/checkbox)
- 优化crud组件弹出表单字典缓冲机智，dicFlag是否拉去新字典[在线例子](/doc/crud/crud-dic)
- 新增input-number组件的placeholder属性[在线例子](/doc/component/input-number)
- 新增cascader组件同步el部分参数[在线例子1](/doc/form/form-cascader),[在线例子2](/doc/component/cascader)
- 修复search组件无法显示字典的问题[在线例子](/doc/search)


## 2.6.5
### 2020-06-22
### 更新
- 修复了select和upload为单图的2个核心问题
### 说明
- avue为了让大家更加省力，近期重构了众多核心代码，感谢你的支持和理解

## 2.6.4
### 2020-06-22
### 更新
- <font color="red">重构了网络字典和本地字典的逻辑dicData和dicUrl可以同时存在，合并处理[在线例子](/doc/form/form-dic)</font>
- 新增switch组件同步el部分参数[在线例子](/doc/component/switch)
- 新增upload组件data和headers参数[在线例子](/doc/form/form-upload)
- 新增checkbox组件全选all参数，默认为true[在线例子](/doc/component/checkbox)
- 新增input-tree组件的父子节点例子[在线例子](/doc/component/input-tree)
- 修复upload组件picture-img类型回调数据问题[在线例子](/doc/form/form-upload)
- 修复crud组件日期部分首次加载问题[在线例子](/doc/crud/crud-date)
- 修复select组件远程加载首次不加载字典的问题,并且同步了select部分属性[在线例子](/doc/form/form-select-remote)
- 修复select多级联动重复请求的问题[在线例子](/doc/form/form-select)
### 说明
- 完善了一部分的文档


## 2.6.3
### 2020-06-20
### 更新
- <font color="red">重构了crud和form组件的字典逻辑</font>
- <font color="red">重构了form组件的初始化生命周期</font>
- <font color="red">[crud搜索出现折叠小箭头](https://gitee.com/smallweigit/avue/issues/I1KL8Y)</font>
- 新增crud组件权限控制menu的字段[在线例子](/doc/crud/crud-permission),[gitee_I1L35Z](https://gitee.com/smallweigit/avue/issues/I1L35Z)
- 新增crud组件headerDragend表头拖动事件[gitee_I1KO6P](https://gitee.com/smallweigit/avue/issues/I1KO6P)
- 新增crud组件的搜索search双向变量[在线例子](/doc/crud/crud-search)
- 新增crud组件分页background和layout配置属性[在线例子](/doc/crud/crud-page)
- 新增crud组件分页prevClick和nextClick上下页的事件[在线例子](/doc/crud/crud-page)
- 新增tree组件自定义卡槽[在线例子](/doc/tree)
- 新增form组件tabsActive属性和[在线例子](/doc/form/form-tabs),[gitee_I1L6WP](https://gitee.com/smallweigit/avue/issues/I1L6WP)
- 新增input-tree组件leafOnly是否只是叶子节点,includeHalfChecked是否包含半选节点属性[在线例子](/doc/component/input-tree)
- 修复input-tree组件自定义使用方法[在线例子](/doc/form/form-compoent-slot)
- 修复curd组件取消按钮报错的问题[gitee_I1KXHA](https://gitee.com/smallweigit/avue/issues/I1KXHA),[gitee_I1KLQA](https://gitee.com/smallweigit/avue/issues/I1KLQA)
- 修复crud组件复杂表头字典失效的问题[在线例子](/doc/crud/crud-headers),[gitee_I1KVUM](https://gitee.com/smallweigit/avue/issues/I1KVUM)
- 修复form组件验证规则的trigger失效问题[gitee_I1L0HH](https://gitee.com/smallweigit/avue/issues/I1L0HH)

- 修复input-number组件的赋值问题 
### 说明
- <font color="red">el请采用最新版本2.13.2，否则会出现未知问题</font>


## 2.6.2
### 2020-06-12
### 更新
- 新增crud组件searchClearable属性[gitee_I1JX2Y](https://gitee.com/smallweigit/avue/issues/I1JX2Y)
- 修复crud组件filter窗口无按钮问题[gitee_I1JZ1A](https://gitee.com/smallweigit/avue/issues/I1JZ1A)
- 修复dynamic组件多个时无法提交的问题[gitee_I1K375](https://gitee.com/smallweigit/avue/issues/I1JZ1A)
- 修复time组件crud搜索时的searchRange无法生效问题[gitee_I1KAR2](https://gitee.com/smallweigit/avue/issues/I1KAR2)
- 新增form和crud组件获取prop组件的对象getPropRef方法[在线例子1](/doc/crud/crud-ref),[在线例子2](/doc/form/form-ref),[gitee_I1K9I3](https://gitee.com/smallweigit/avue/issues/I1K9I3)
- 修复crud组件级联字典的问题[gitee_pr_19](https://gitee.com/smallweigit/avue/pulls/19)

## 2.6.1
### 2020-06-06
### 更新

- 修复form组件多级联动的赋值问题[在线例子](/doc/form/form-select),[gitee_I1J4M7](https://gitee.com/smallweigit/avue/issues/I1J4M7)
- 修复form组件vaildate内置方法[在线例子](/doc/form/form),[gitee_I1JG95](https://gitee.com/smallweigit/avue/issues/I1JG95)
- 修复form组件display逻辑问题
- 修复crud组件searchMenuSpan失效问题[在线例子](/doc/crud/crud-search),[gitee_I1IZT8](https://gitee.com/smallweigit/avue/issues/I1IZT8)
- 修复crud组件表单滚动条的问题[gitee_I1JHFG](https://gitee.com/smallweigit/avue/issues/I1JHFG)
- 修复crud组件自适应高度的问题
- 修复crud组件切换表格的显隐问题[在线例子](/doc/crud/crud-change)，[github_263](https://github.com/nmxiaowei/avue/issues/263)
- 新增cascader组件emitPath参数[gitee_I1J6FA](https://gitee.com/smallweigit/avue/issues/I1J6FA)
- 新增crud组件表单dialogHeight为'auto'自适应屏幕属性
- 新增form组件级联动cascaderIndex默认选择项[在线例子](/doc/form/form-select)
- 新增upload组件picture-img模式下支持base64[gitee_I1J4M7](https://gitee.com/smallweigit/avue/issues/I1J4M7)
- 删除crud组件displayAs的开关属性

## 2.6.0
### 2020-05-30
### 更新

- [CRUD最强封装-极简增删改查（你怕了吗？)](/doc/crud-fun)
- <font color="red">重构form组件多级联动的使用方法（可以各行配置，不用配置dicFlag属性）[gitee_I1IGTG](https://gitee.com/smallweigit/avue/issues/I1IGTG),[在线例子](/doc/form/form-select)</font>
- <font color="red">新增form组件分组模式的折叠显示，新增2个参数arrow（控制折叠）和collapse（默认叠起）[在线例子](/doc/form/form-group)</font>
- <font color="red">优化crud组件permission的用法，新增回调函数[在线例子](/doc/crud/crud-permission)</font>
- <font color="red">修复input-tree组件的父子包含关联[gitee_I1GFTB](https://gitee.com/smallweigit/avue/issues/I1GFTB),[在线例子](/doc/component/input-tree)</font>
- 新增flow组件的用法和优化[在线例子](/doc/flow),[gitee_I1IKPA](https://gitee.com/smallweigit/avue/issues/I1IKPA)
- 新增upload组件从查看模式下触发uploadPreview回调函数[gitee_I1II7X](https://gitee.com/smallweigit/avue/issues/I1II7X),[github_255](https://github.com/nmxiaowei/avue/issues/255)
- 新增crud组件searchSize属性[gitee_I1HMO7](https://gitee.com/smallweigit/avue/issues/I1HMO7),[在线例子](/doc/crud/crud-search)
- 新增select组件的resKey和formatter方法[gitee_I1H3OQ](https://gitee.com/smallweigit/avue/issues/I1H3OQ)
- 修复crud表格搜索按钮的问题[gitee_I1IPWY](https://gitee.com/smallweigit/avue/issues/I1IPWY)
- 新增form组件的focus和blur事件的回调传值[在线例子](/doc/form/form-event)
- 修复affix组件的样式问题导致失效[gitee_I1HKBM](https://gitee.com/smallweigit/avue/issues/I1HKBM),[在线例子](/doc/affix)
- 修复crud组件menuBtnTitle属性失效问题[gitee_I1HJ79](https://gitee.com/smallweigit/avue/issues/I1HJ79)
- 修复crud组件cell行编辑模式数据校验的问题[gitee_I1FSXM](https://gitee.com/smallweigit/avue/issues/I1FSXM),[在线例子](/doc/crud/crud-cell)
- 新增dynamic组件的数据校验规则配置[gitee_I1FJ03](https://gitee.com/smallweigit/avue/issues/I1FJ03),[在线例子1](/doc/form/form-dynamic),[在线例子2](/doc/crud/crud-dynamic)
- 修复form组件error回调函数的问题[gitee_I1FHSO](https://gitee.com/smallweigit/avue/issues/I1FHSO)
- 修复upload组件的accept属性方法限制问题[github_245](https://github.com/nmxiaowei/avue/issues/245)
- 修复datetime组件的数据类型问题[github_242](https://github.com/nmxiaowei/avue/issues/242)
- 修复upload禁止时上传按钮显示问题
- 修复input组件错误类型的问题[gitee_I1HTZD](https://gitee.com/smallweigit/avue/issues/I1HTZD)
- 优化没引入axios包的警告提示和变量名由$httpajax变为$axios
- 重构form组件和crud组件的rule校验函数逻辑

### 说明
- <font color="red">版本变动较大，谨慎更新，更新不易，且行且珍惜！！！</font>
- <font color="red">[文档贡献说明](/doc/pr)</font>


## 2.5.3
### 2020-05-17
### 更新

- [CRUD最强封装-极简增删改查（你怕了吗？)](/doc/crud-fun)
- <font color="red">优化imagePreview组件，增加了放大缩小旋转等功能[在线例子1](/doc/image-preview)[在线例子2](/doc/crud/crud-url)</font>
- <font color="red">新增el的原生dialog组件拖拽指令v-dialogdrag[在线例子](/doc/crud/crud-dialogDrag)</font>
- <font color="red">新增crud组件布局重绘方法doLayout和refreshTable方法[gitee_I1GSWJ](https://gitee.com/smallweigit/avue/issues/I1GSWJ)</font>
- <font color="red">重构input-tree组件的核心方法[在线例子](/doc/form/form-tree)</font>
- <font color="red">重构tree组件的核心方法[在线例子](/doc/tree)</font>
- <font color="red">删除了backTop组件，由于el官网已经提供[ele在线例子](https://element.eleme.cn/#/zh-CN/component/backtop)</font>
- 新增form组件的列单独配置detail样式[在线例子](/doc/form/form-detail)
- 新增upload组件httpProps对象的home属性，图片的根路径[在线例子](/doc/form/form-upload)
- 新增input-tree组件的classIcon图标属性
- 新增upload组件阿里云oss其它配置属性[在线例子](/doc/form/form-upload-ali),[gitee_I1G74O](https://gitee.com/smallweigit/avue/issues/I1G74O)
- 新增tree组件全部的eltree内置方法[github_122](https://github.com/nmxiaowei/avue/issues/122)
- 修复form组件readonly属性不起作用的问题[gitee_I1H3PR](https://gitee.com/smallweigit/avue/issues/I1H3PR)
- 修复crud组件表格拖拽新增数据的问题[在线例子](/doc/crud/crud-sortable)
- 修复crud组件行编辑新增错误问题[在线例子](/doc/crud/crud-cell)
- 修复crud组件隐藏时header属性的问题[github_173](https://github.com/nmxiaowei/avue/issues/173)
- 修复crud组件核心显示方法detail的问题[gitee_I1GKLT](https://gitee.com/smallweigit/avue/issues/I1GKLT)
- 修复crud组件列拖拽的问题[在线例子](/doc/crud/crud-column-sortable)
- 修复upload组件在表格的展示问题
- 修复input-tree组件全选只保存叶子节点的问题[gitee_I17K9A](https://gitee.com/smallweigit/avue/issues/I17K9A)
- 修复input-tree组件字典更新不翻译的问题
- 修复img组件数据为空的错误问题[gitee_I1GQ80](https://gitee.com/smallweigit/avue/issues/I1GQ80)
- 修复flop和text大屏组件的部分问题 [在线例子](https://data.avuejs.com)
- 优化draggable组件，增加键盘移动事件[在线例子](/doc/draggable)
- 优化form组件的enter回车提交表单属性默认为false
- 优化crud组件表格显隐组件的交互[在线例子](/doc/crud/crud-column-sortable)[github_237](https://github.com/nmxiaowei/avue/issues/237)

### 说明
- <font color="red">新增通用模板代码[在线例子](/doc/temp)</font>
- <font color="red">这个版本修复bug和优化很多，强烈推荐升级</font>
- <font color="red">尤其是重构了input-tree组件和tree组件的核心方法</font>
- <font color="red">upload组件增加根路径home配置属性</font>


## 2.5.2
### 2020-05-03
### 更新
- <font color="red">删除了单独的detail组件</font>
- 新增form组件的detail详情属性，编辑和详情一键转换[在线例子](/doc/form/form-detail)
- 新增form组件全局控制列属性(disabled,readonly,span)3个属性
- 新增form组件tabs和group的一键转换,可以用于打印整体效果[在线例子](/doc/form/form-tabs)
- 新增pay数据展示组件的tip提示[在线例子](/doc/data/data0)
- 修复crud组件搜索按钮不换行的样式问题[在线例子](/doc/crud/crud-search)
- 优化crud查看按钮时的详情样式
- 修复crud组件typeList为picture-img列单图预览问题[在线例子](/doc/crud/crud-url)


## 2.5.1
### 2020-04-30
### 更新
## [求start~.~最新插件-JChat聊天组件](https://gitee.com/CodeGI/chat)
- <font color="red">【重要更新】增加在option配置中引入任意组件功能[在线例子](/doc/form/form-component)</font>
- <font color="red">【重要更新】新增$Print全局打印方法，可以局部打印网页特定部分，同时crud和form组件内置printBtn属性[在线例子](/doc/print),[在线例子1](/doc/crud/crud-print),[在线例子2](/doc/form/form)</font>
- <font color="red">【重要更新】优化了$imagePreview图片展示问题[gitee_I16LSF](https://gitee.com/smallweigit/avue/issues/I16LSF),[在线例子](/doc/image-preview)</font>
- <font color="red">【重要更新】avue-input名字组件变成为avue-input-tree组件[在线例子](/doc/component/input-tree)</font>
- <font color="red">【重要更新】新增crud组件中upload组件时,表格列显示图片[在线例子](/doc/crud/crud-url)</font>
- <font color="red">【重要更新】修改crud组件弹窗dialog的样式，新增全局样式avue-dialog可以作用域任意el的dialog[在线例子](/doc/api),[gitee_I1FAXS](https://gitee.com/smallweigit/avue/issues/I1FAXS)</font>
- 新增全局配置theme主题配置，目前只有dark黑色主题[在线例子](/doc/window)
- 新增icon-select组件可以配置name名称[在线例子](/doc/form/form-icon-select)
- 新增form组件group和tabs新增加头体结构[gitee_I1EKGI](https://gitee.com/smallweigit/avue/issues/I1EKGI)
- 新增crud组件所有按钮和标题的文案配置[在线例子](/doc/crud/crud-text)
- 新增crud组件dialogDestroy属性[gitee_I1FKOB](https://gitee.com/smallweigit/avue/issues/I1FKOB)
- 优化mock属性变更为mockBtn[在线例子](/doc/form/form-mock)
- 优化crud组件simplePage:true时仍占用布局空间问题[在线例子](/doc/crud/crud-search),[gitee_pr_18](https://gitee.com/smallweigit/avue/pulls/18)
- 优化crud组件的reserveSelection，多选模式下，翻页可以记录上页保存的[在线例子](/doc/crud/crud-selection),[gitee_I1EKGI](https://gitee.com/smallweigit/avue/issues/I16YG8)
- 优化crud内部所有按钮文案的配置[在线例子](/doc/crud/crud-text)
- 修复了crud组件内置方法rowSave和rowUpdate无法使用问题
- 修复crud组件和form组件清空时候无法清空默认值的情况[gitee_I1FK8D](https://gitee.com/smallweigit/avue/issues/I1FK8D)
- 修复crud组件搜索组件的日期type问题[github_224](https://github.com/nmxiaowei/avue/issues/224)
- 修复crud组件搜索属性searchMmultiple不生效问题[gitee_I1ER94](https://gitee.com/smallweigit/avue/issues/I1ER94)
- 修复crud组件内置方法updateDic无法使用问题[github_219](https://github.com/nmxiaowei/avue/issues/219)
- 修复input-tree组件多选的清空不清楚勾选的问题[github_204](https://github.com/nmxiaowei/avue/issues/204)
- 修复select组件下拉多选样式问题[gitee_I1FASH](https://gitee.com/smallweigit/avue/issues/I1FASH)
- 修复login组件发送短信倒计时问题[github_220](https://github.com/nmxiaowei/avue/issues/220)
- 修复了getUrlParams方法问题[github_218](https://github.com/nmxiaowei/avue/issues/218)

### 说明
- <font color="red">这个版本修复bug和优化很多，强烈推荐升级，尤其是自定义三方组件</font>
- <font color="red">富文本插件和地图插件前面需要加上前缀如compoent:avue-xxx</font>
- <font color="red">由于样式问题导致searchMenuSpan属性无法使用,需要添加如下样式，下个版本修复</font>

```
.avue-form__menu{
	width:inherit
}
```


## 2.5.0
### 2020-04-10
### 更新
- <font color="red">版本变动较大，谨慎更新，更新不易，且行且珍惜！！！</font>
- <font color="red">【重要更新】删除了findColumnIndex方法，改成findObject全局方法[在线例子](/doc/form/form-display) </font>
- <font color="red">【重要更新】新增crud组件的save,update,del函数的done函数传入新数据，达到局部刷新数据效果(表格树也可以哦)</font>[gitee_I1AA8R](https://gitee.com/smallweigit/avue/issues/I1AA8R)
- <font color="red">【重要更新】新增crud组件全部按钮的文案配置，例如新增按钮addBtnText、编辑按钮editBtnText等[在线例子](/doc/crud/crud-text)</font>
- <font color="red">【重要更新】新增dynamic子表单组件卡槽</font>[在线例子1](/doc/form/form-dynamic),[在线例子2](/doc/crud/crud-dynamic),[gitee_I1BNA1](https://gitee.com/smallweigit/avue/issues/I1BNA1)
- 新增crud组件的search卡槽[在线例子](/doc/crud/crud-search),[gitee_pr_16](https://gitee.com/smallweigit/avue/pulls/16)
- 新增crud组件的seachMenu卡槽回调row表单参数[gitee_I1DBWE](https://gitee.com/smallweigit/avue/issues/I1DBWE)
- 新增crud组件的动态显隐列的columnShow参数[在线例子](/doc/crud/crud-showcolumn),[gitee_I1C9V0](https://gitee.com/smallweigit/avue/issues/I1C9V0)
- 新增form组件error回调函数，返回当前校验不通过的字段[在线例子1](/doc/form/form-rules),[在线例子2](/doc/crud/crud-rules)
- 新增cascader组件的typeslot组件卡槽和其他属性[在线例子](/doc/form/form-cascader)
- 新增upload组件自动判断图片和视频类型，废除params参数[在线例子](/doc/form/form-upload)
- 新增upload组件的uploadError错误回调方法[在线例子](/doc/form/form-upload)
- 新增notice组件的click点击事件[在线例子](/doc/notice),[gitee_I1CLTY](https://gitee.com/smallweigit/avue/issues/I1CLTY)
- 新增data-tabs数据组件支持精度属性[github_209](https://github.com/nmxiaowei/avue/issues/209),[github_pr_177](https://github.com/nmxiaowei/avue/pull/177)
- 修复number组件的step失效问题[github_186](https://github.com/nmxiaowei/avue/issues/186)
- 修复textarea组件搜索时候的显示问题
- 修复data-panel组件的decimals属性类型问题
- 修复crud组件filterBtn时时间选择器的样式[gitee_I1C3CA](https://gitee.com/smallweigit/avue/issues/I1C3CA)
- 修复crud使用tabs时的失效问题和多一个空白卡槽的问题[github_212](https://github.com/nmxiaowei/avue/issues/212),[github_211](https://github.com/nmxiaowei/avue/issues/211)
- 修复crud组件分页的问题,如果total为0分页将不会显示
- 修复crud组件导出excel文件名时间戳的问题


### 说明
- <font color="red">配置simplePage分页为1的时候不显示属性,默认为false</font>[github_197](https://github.com/nmxiaowei/avue/issues/197)
- <font color="red">使用crud组件表格树、多选、等要先配置rowKey主键属性，默认为id</font>[gitee_I1C88J](https://gitee.com/smallweigit/avue/issues/I1C88J)



## 2.4.1
### 2020-03-17
### 更新
- 新增chat组件支持语音[在线例子](/doc/chat)
- 新增curd组件的simplePage分页为1的时候不显示属性,默认为false[在线例子](/doc/crud/crud-page)
- 新增form组件支持tabs选项卡分组[在线例子](/doc/form/form-tabs),[gitee_I18NZV](https://gitee.com/smallweigit/avue/issues/I18NZV)
- 新增upload组件删除时候upload-delete方法回调删除的file文件[在线例子](/doc/form/form-upload),[gitee_I1A1XN](https://gitee.com/smallweigit/avue/issues/I1A1XN)
- 新增了一个骚操作[在线例子](/doc/sao)
- 修复了crud组件的height为auto时的calcHeight失效属性
- 优化了echart图表组件的各种问题
- 优化分页删除时空数据时的页码问题[gitee_I1AI5C](https://gitee.com/smallweigit/avue/issues/I1AI5C)

- <font color="red">curd的before-open方法赋值例子[在线例子](/doc/crud/crud-open)</font>
- <font color="red">修改crud分页逻辑，一页的时候不显示分页组件(/doc/crud/crud-open)</font>
### 说明
- <font color="red"> 子表单和树型表格出现分页组件的配置simplePage:true即可隐藏</font>


## 2.4.0
### 2020-02-17
### 更新
- 优化了License组件的部分功能[在线例子](/doc/license)
- 优化了form和crud组件的多级联动[在线例子](/doc/form/form-select)
- 优化了crud组件的搜索和表格共同字典减少请求[gitee_I19CZS](https://gitee.com/smallweigit/avue/issues/I19CZS)
- 优化了crud和form组件的绑定变量逻辑
- 优化了crud的dateBtn日期组件和样式布局调整[在线例子](/doc/crud/crud-datetime)
- 调整了全局组件大小的默认值为small,当然你也可以设置为medium，你也可以单独为组件配置size属性[在线例子](/doc/crud/crud)，[全局配置](/doc/window)
- 优化了若干组件细节
- <font color="red">curd的before-open方法赋值例子[在线例子](/doc/crud/crud-open)</font>
- <font color="red">修改crud分页逻辑，一页的时候不显示分页组件(/doc/crud/crud-open)</font>
### 说明
疫情期间为了让大家学习，其他组件模块文档全部免费开放～。～

## 2.3.8
### 2020-01-20
### 更新
- 新增crud组件分页选择器的page变量添加pagerCount超出多少隐藏属性，同时添加分页选择器sync修饰符[在线例子](/doc/crud/crud-page)
- 新增crud和form组件的label和error卡槽[gitee_I17WGC](https://gitee.com/smallweigit/avue/issues/I17WGC)，[在线例子1](/doc/crud/crud-formslot)，[在线例子2](/doc/form/form-slot)
- 优化crud组件搜索reset-change回调函数
- 修复crud组件dialog为drawer属性时的全屏问题
- 删除curd组件重复定义的属性
- 删除tree组件删除时的提示信息，可以在删除回调函数自行添加[gitee_I18KWV](https://gitee.com/smallweigit/avue/issues/I18KWV),[在线例子](/doc/tree)
### 说明
 <font color="red">提前祝大家新年快乐～。～，我们年后再战 Go!Go!Go!</font>



## 2.3.7
### 2020-01-10
### 更新
- 修复crud搜索清空内存溢出的问题[gitee_I17XCQ](https://gitee.com/smallweigit/avue/issues/I17XCQ)
- 修复crud一个监听问题导致的一系列问题[gitee_I16XPP](https://gitee.com/smallweigit/avue/issues/I16XPP)
- 修复官网文档一些错误的写法
- 重构了chat聊天组件[在线例子](/doc/chat)


## 2.3.6
### 2020-01-06
### 更新
- <font color="red">新增单组件使用文档（持续更新）</font>[在线例子](/doc/component/input)
- <font color="red">新增dicFormatter函数，可以自定义返回字典的结构层级</font>[在线例子](/doc/form/form-select)
- 新增crud和form组件enter回车是否提交表单属性，默认为true
- 新增dynamic子表单的rowAdd和rowDel回调事件[在线例子](/doc/form/form-dynamic)
- 新增searchRules配置搜索的验证规则[在线例子](/doc/crud/crud-search)
- 修复清空搜索栏函数searchReset失效问题[gitee_I17O6C](https://gitee.com/smallweigit/avue/issues/I17O6C)
- 修复crud字典resKey配置不生效问题[gitee_I17KA2](https://gitee.com/smallweigit/avue/issues/I17KA2)
- 修复crud组件中的saveBtn和updateBtn部分属性失效问题
### 说明
- <font color="red">2020年的第一个版本，祝大家新年快乐～。～</font>


## 2.3.5
### 2019-12-31
### 更新
- 修复了一些奇奇怪怪的样式问题
### 说明
- <font color="red">由于测试的失误，和大家说一声抱歉</font>


## 2.3.4
### 2019-12-30
### 更新
- <font color="red">新增单组件使用文档（持续更新）</font>[在线例子](/doc/component/input)
- <font color="red">新增searchMenuSpan属性可以调节搜索按钮是否单独成行</font>[在线例子](/doc/crud/crud-search)
- 优化了crud组件的viewBtn查看界面的样式问题[gitee_I178HA](https://gitee.com/smallweigit/avue/issues/I178HA)
- 优化了不引入axios包的友好提示问题
- 优化了license组件可以配置图片和自定义卡槽[在线例子](/doc/license)
- 重构了input-tree树形框组件[gitee_I17AA7](https://gitee.com/smallweigit/avue/issues/I17AA7）
- 新增了calcHeight属性，配置height:'auto'时候高度差使用，可以使表格任何分辨率下都全屏充满[gitee_I17BIF](https://gitee.com/smallweigit/avue/issues/I17BIF)
- 新增crud内置的弹出表单带全屏窗口功能
- 新增curd的fit是列的宽度是否自撑开属性
- 新增upload组件的params参数，可以动态赋值参数,可以支持视频[在线例子](/doc/form/form-upload)
- 新增upload组件的upload-preview方法，可以调用查看前执行的方法[在线例子](/doc/form/form-upload)
- 新增input组件type为tree时单选disabled不起作用[gitee_I176FY](https://gitee.com/smallweigit/avue/issues/I176FY)
- 新增tree组件的懒加载使用[在线例子](/doc/tree)
- 新增form组件group分组之外可以配置其他column[在线例子](/doc/form/form-group),[gitee_I1767V](https://gitee.com/smallweigit/avue/issues/I1767V)
- 修复display属性对crud搜索配置的影响[gitee_I16PCJ](https://gitee.com/smallweigit/avue/issues/I16PCJ)
- 修复input组件type为tree时的空值报错问题[gitee_I16QT4](https://gitee.com/smallweigit/avue/issues/I16QT4)
- 修复crud组件的$cellEdit赋值问题[gitee_I16KXW](https://gitee.com/smallweigit/avue/issues/I16KXW)
- 修复了crud组件中调用loading防重提交的失效问题[gitee_I17A7X](https://gitee.com/smallweigit/avue/issues/I17A7X)
- 修复了datetime组件的验证规则取值问题[gitee_I1745](https://gitee.com/smallweigit/avue/issues/I17A7X)

### 说明
- <font color="red">想搜索按钮是否单独成行解决方案，想的头都秃噜皮了～。～</font>


## 2.3.3
### 2019-12-23
### 更新
- 新增upload组件的upload-delete方法，可以调用删除前执行的方法[在线例子](/doc/form/form-upload),[gitee_I16ONN](https://gitee.com/smallweigit/avue/issues/I16ONN)
- 新增value和searchValue属性(也就是表单和搜索框的默认值)[在线例子](/doc/crud/crud-search),[gitee_I170YS](https://gitee.com/smallweigit/avue/issues/I170YS)
- 修复了array组件的disabled和readonly的属性问题
- 优化了curd组件中form表单的使用底层的组件，支持menuPosition属性[在线例子](/doc/crud/crud-menubtn)
### 说明
- <font color="red">为了更好的代码规范和骚功能版本改动较大，大家谨慎升级生产环境</font>
- <font color="red">valueDefault和searchDefault为了更好的语意做了属性变更</font>
- <font color="red">curd中弹出表单的高度默认为自适应，当然超出屏幕的化，你可以自定义dialogHeight设置高度</font>

## 2.3.2
### 2019-12-13
### 更新
- 修复了crud组件的搜索获取组件的问题
- 删除了valueDefault和searchDefault属性（由于得层的改动，这个版本先删除，下个版本将会再加上）
### 说明
删除了valueDefault属性，如果需要赋值的话，请使用v-model属性，是crud组件的话要在before-open里setTimeout赋值即可

## 2.3.1
### 2019-12-13
### 更新
- 新增搜索属性日期范围属性searchRange[gitee_I16LSL](https://gitee.com/smallweigit/avue/issues/I16LSL)
- 新增upload单图上传时的删除和查看菜单[gitee_I16MAQ](https://gitee.com/smallweigit/avue/issues/I16MAQ)
- 新增upload组件上传时候uploadBefore对file文件修改的方法[在线例子](/doc/form/form-upload),[gitee_I16B38](https://gitee.com/smallweigit/avue/issues/I16B38)
- 扩展keyboard键盘组件的使用[在线例子](/doc/keyboard),[github_pr_143](https://github.com/nmxiaowei/avue/pull/143)
- 扩展了chat客服组件使用[在线例子](/doc/chat)
- 修复uplod组件上传时的空报错问题[gitee_I15O2P](https://gitee.com/smallweigit/avue/issues/I15O2P)
- 修复了crud的filter功能的部分问题，并且改为form组件底层
- 修复input-tree组件多选时候不回显问题[gitee_I16O1Q](https://gitee.com/smallweigit/avue/issues/I16O1Q)
- 修复了input-tree组件的defaultExpandAll使用问题
- 修复data数据组件的decimals小数点位数问题默认为0[gitee_I16MAG](https://gitee.com/smallweigit/avue/issues/I16MAG)
- 修复crud搜索tree不显示的问题[github_139](https://github.com/nmxiaowei/avue/issues/139)
- 修复crud搜索searchSubBtn，searchResetBtn属性失效问题[gitee_I16JQ8](https://gitee.com/smallweigit/avue/issues/I16JQ8)


## 2.3.0
### 2019-12-09
### 更新
- <font color="red">重构了crud组件的搜索部分，采用form组件的作为底层</font>
- 新增crud组件的搜索支持自定义组件[在线例子](/doc/crud/crud-search-slot)
- 新增crud组件的搜索部分的多级联动,表格支持级联动的翻译[在线例子](/doc/crud/crud-select)
- 新增crud组件的tree懒加载功能[在线例子](/doc/crud/crud-tree-lazy),[github_134](https://github.com/nmxiaowei/avue/issues/134)
- 新增crud组件的clearSort清除排序方法[github_131](https://github.com/nmxiaowei/avue/issues/131)
- 新增crud组件和form服务端动态加载配置[在线例子](/doc/crud/crud-ajax)
- 新增crud组件height属性为‘auto’时，自动填充满表格底部
- 新增input组件的tree懒加载功能[在线例子](/doc/form/form-tree-lazy)
- 新增form组件回车提交表单功能
- 新增dynamic组件的addBtn、delBtn、viewBtn的 属性配置，默认为true
- 新增datetime和time组件的unlinkPanels方法[gitee_I15WKC](https://gitee.com/smallweigit/avue/issues/I15WKC)
- 新增form组件labelSuffix配置label后缀字符串，默认：
- 修复type为img时候dataType失效问题
- 修复validatenull方法对0判断的问题
- 修复labelWidth设置为0的失效问题

### 说明
-  更新到element-ui2.12.0+版本
-  <font color="red">crud搜索有了重大的改变，采用form组件作为底层，理论上支持form组件的全部功能，当然你也可以自定义其中的某一个搜索列</font>
-  <font color="red">crud搜索回掉的时候需要和form组件提交一样，调用done函数结束调用searchChange(form,done)=>{done();}</font>

## 2.2.3
### 2019-11-25
### 更新
- 新增url/img/array组件[在线例子](/doc/new/form-url)
- 新增upload组件的headers属性
- 新增uplad组件的data属性
- 修复了notic消息通知组件的样式问题[在线例子](/doc/notice)
- 扩展了video视频组件的用法[在线例子](/doc/video)

### 说明
- dataType属性数据的类型转换也就是数组和用逗号隔开的字符串（适用于checkbox/select多选/tree多选/img/array/upload）[详细说明](/doc/dataType)
- 兼容最新版的element-ui(2.12.0+)

## 2.2.2
### 2019-11-11
### 更新
- 修复一个十分严肃的问题！！！

## 2.2.1
### 2019-11-08
### 更新
- 新增crud组件和form组件的translate（配置translate为false时可以在提交时过滤$字段和空字段）属性[github_112](https://github.com/nmxiaowei/avue/issues/112)
- 修复upload组件时label显示问题
### 说明
- 感谢B站UP录制的avue相关例子视频[点击直达](/doc/learn/1-1)

## 2.2.0
### 2019-11-04
### 更新
- 新增login登录组件[在线例子](/doc/login)
- 新增keyboard键盘组件[在线例子](/doc/keyboard)
- 新增notice消息通知组件[在线例子](/doc/notice)
- 新增crud组件的日期组件[在线例子](/doc/crud/crud-datetime)
- 新增crud组件列的拖拽排序[在线例子](/doc/crud/crud-column-sortable)
- 新增tree组件的defaultCheckedKeys和defaultExpandedKeys属性
- 新增form组件阻止submit的prevent事件
- 新增upload组件的data附加参数属性 
- 新增datetime组件和date组件的defaultValue和defaultTime属性
- 修复crud组件的部分样式问题
- 修复flow组件的赋值px问题
- 优化form底层模版的核心组件

## 2.1.5
### 2019-10-10
### 更新
- 修复底层form核心组件

## 2.1.4
### 2019-10-09
### 更新
- 修复底层form核心组件

## 2.1.3
### 2019-10-08
### 更新
- 新增dynamic组件自表单上传空间可以使用（和form组件使用方法一致）
- 修复crud组件时间空间日期范围报错问题
- 修复了dynamic自表单组件的若干潜在问题
- 修复icon-select组件很多组件高度问题[issues292](https://git.avuejs.com/avue/avuex/issues/292)
- 优化了form组件以及curd组件的cell模式的底层公用问题


## 2.1.2
### 2019-09-23
### 更新
- 修复flow组件的lineList不赋值问题[github_99](https://github.com/nmxiaowei/avue/issues/99)
- 修复tree赋值清空value时label不清空的问题[issues279](https://git.avuejs.com/avue/avuex/issues/279)

## 2.1.1
### 2019-09-20
### 更新
- 新增screenshot屏幕截图API方法[在线例子](/doc/screenshot)
- 新增crud组件的selectable方法
- 新增select树形下拉框的defaultExpandAll全部展开方法[issues266](https://git.avuejs.com/avue/avuex/issues/266)
- 新增crud组件搜索的searchSpan,searchGutter,searchLabelWidth三个参数
- 优化了脚手架的构建
- 修复tree组件弹窗时的遮照问题[issues275](https://git.avuejs.com/avue/avuex/issues/275)
- 修复datetime类型不触发change事件[issues269](https://git.avuejs.com/avue/avuex/issues/269)
- 修复了部分crud的样式问题
- 修复crud组件使用dynamic时不加载的问题
- 修复flow组件的option部分值不赋值问题[github_95](https://github.com/nmxiaowei/avue/issues/95)
- 修复了核心字典方法部分不翻译的问题


## 2.1.0
### 2019-08-30
### 更新
- 优化dynamic为form的子表单[在线例子](/doc/form/form-dynamic)
- 新增video摄像头组件[在线例子](/doc/video)
- 新增verify验证码组件[在线例子](/doc/verify)
- 新增tree和select组件多自定义组件卡槽[在线例子](/doc/form/form-compoent-slot)
- 新增select分组功能[在线例子](/doc/form/form-select-group)
- 新增select多选拖拽排序[在线例子](/doc/form/form-select-drag)
- 新增radio和checkbox样式配置属性[在线例子](/doc/form/form-compoent-btn)
- 新增excel导入全局api[issues245](https://git.avuejs.com/avue/avuex/issues/245)，[在线例子](/doc/xlsx)
- 新增rate组件的texts辅助文字属性[issues243](https://git.avuejs.com/avue/avuex/issues/243)
- 新增搜索栏目的searchSpan方法
- 新增upload组件上传前后方法返回column参数[issues249](https://git.avuejs.com/avue/avuex/issues/249)
- 修复silder组件拼写错误silder->slider[issues244](https://git.avuejs.com/avue/avuex/issues/244)
- 修复crud组件行编辑时的问题
- 修复了tree组件的样式和查询效率
- 修复detail组件无法识别html代码片段[issues249](https://git.avuejs.com/avue/avuex/issues/246)
- 修复form组件menuPostion->menuPosition[issues237](https://git.avuejs.com/avue/avuex/issues/237)
- 优化crud的内置更新方法updateDic支持空数组[issues235](https://git.avuejs.com/avue/avuex/issues/235)
- 优化crud组件内容的dialogType为drawer时改为ele原生组件，新增dialogDirection抽屉方向属性[在线例子](/doc/crud/crud-direction)
- 优化了多重字典查询的核心方法
- 优化了crud多选时tip的样式
- 删除了table-tree历史树组件
- 删除了time-line时间轴组件
- 删除了divider分割线组件
- 删除了drawer抽屉组件
### 说明
- 感谢@SSC(https://github.com/sscfaith)贡献curd新版搜索样式和表单设计工具
- 表单设计器[(https://form.avuejs.com)](https://form.avuejs.com)和表格设计器[(https://crud.avuejs.com)](https://crud.avuejs.com)
- antd的版本已经在路上了哦～。～
- 删除大部分组件，将采用ele原生组件代替
- 请将ele升级到2.11.0版本，否则无法正常使用


## 2.0.6
### 2019-08-04
### 更新
- 新增左树右表[在线例子](/doc/crud/crud-table-tree)
- 新增curd组件弹出框自定义按钮saveBtn和updateBtn以及cancelBtn配置[在线例子](/doc/crud/crud-btn)
- 修复tree组件防止防止表单提交问题[IZWNZ](https://gitee.com/smallweigit/avue/issues/IZWNZ)
- 新增tree组件的loading事件和crud/form用法一致
- 修复tree组件的nodeClick阻止冒泡事件问题
- 新增所有图表新增clickFormatter事件
- 新增crud组件的header卡槽
- 修复crud组件search配置时select属性remote不生效问题
- 修复计算平均数的问题[issues225](https://git.avuejs.com/avue/avuex/issues/225)
### 说明
- 优化了富文本编辑器[在线例子](/doc/plugins/ueditor-plugins)


## 2.0.5
### 2019-07-28
### 更新
- 新增全局nprogress进度条方法[在线例子](/doc/nprogress)
- 新增客服Chat聊天组件[在线例子](/doc/chat)
- 新增License授权书组件[在线例子](/doc/license)
- 新增表单防止重复提交[在线例子](/doc/form/form-submit)
- 新增行单击/双击编辑[在线例子](/doc/crud/crud-click)
- 新增表格树形tree-props配置属性
- 修复crud组件中refresh-change方法返回page为空问题[issues225](https://git.avuejs.com/avue/avuex/issues/225)
- 修复了dialogDrawer高度的问题[issues221](https://git.avuejs.com/avue/avuex/issues/221)
- 修复表格编辑时precision属性失效问题
- 修复crud组件menu右边的按钮样式问题
- 修复表格搜索回车刷新问题
- 新增一款dataPrice数据模版展示组件[在线例子](/doc/data/data12)
- 新增countUp组件decimals精度属性

### 说明
- 新版本的form组件submit方法新增了done方法用来防止重复提交
- 官网新域名启动[http://avuejs.com](http://avuejs.com)


## 2.0.4
### 2019-07-12
### 更新
- 新增Sing组件，用于电子签名和电子盖章[在线例子](/doc/sign)
- 新增crud组件弹窗表单可以拖拽[在线例子](/doc/crud/crud-dialogDrag)
- 新增Flow流程组件，常用于工作流，流程图等其他场景[在线例子](/doc/flow)
- 新增crud弹窗类型属性dialogType(drawer)[在线例子](/doc/crud/crud-drawer)
- 新增crud组件的行和单元格的rowStyle和cellStyle对外函数[在线例子](/doc/crud/crud-style)
- 新增crud组件indexWdth,selectionWidth,expandWidth | indexFixed,selectionFixed,expandFixed相关属性[在线例子](/doc/crud/crud-fixed)
- 优化tree属性的样式和交互方式[在线例子](/doc/form/form-tree)
- 修复textarea属性搜索框是的问题
- 修复tabs数据组件click事件不回传数据的问题
- 新增input的showWordLimit限制字数提示属性


## 2.0.3
### 2019-06-23
### 更新
- 新增全局$export方法excel导出[在线例子](/doc/api)
- 新增upload组件使用阿里oss功能使用临时token的方式stsToken属性
- 新增upload组件上传时自定义文件流名称res配置中fileName属性(默认为file)
- 修复图表组件imgBorder的空值问题
- 新增图表组件line和bar的labelFormatter表格格式化方法
- 修复crud使用最新ele版本多出空白列的问题
- 新增crud的弹出表单clearValidate和validate方法，注意只能在弹出窗后使用
- 新增crud的viewTitle,editTitle,addTitle修改弹出框标题的配置属性
- 新增crud拖拽排序的单独列属性dragHandler属性[在线例子](/doc/crud/crud-sortable)
### 说明
- 已经兼容最新的ele2.9.1+版本


## 2.0.2
### 2019-06-17
### 更新
- 完善watermark水印方法并且增加了调节参数(水印添加后无法取消)[在线例子](/doc/watermark)
- 新增input相关的autocomplete属性
- 修复crud的显隐列的动态选择问题
- 新增图表组件的title标题的相关方法
- 新增upload的删除回调on-remove事件[issues210](https://git.avuejs.com/avue/avuex/issues/210)
- 图表组件legendShow相关属性变更legend
- 全部data数据展示组件新增countup特效的属性animation默认为true
- 新增crud拖拽排序的方法sortable属性(不支持表格树)[在线例子](/doc/crud/crud-sortable)
### 说明
- 为了更好的使用avue，请使用ele2.8.2版本


## 2.0.1
### 2019-06-10
### 更新
- 新增全局水印watermark方法[在线例子](/doc/watermark)
- 修复仪表盘组件无颜色时的报错问题
- 新增地图高亮特效empColor属性
- 修复Draggable组件的鼠标相关事件问题
- 新增图表组件的主题更换功能
- 新增图表组件Funnel漏洞图[在线例子](/doc/echart-funnel)
- 新增图表组件Radar雷达图[在线例子](/doc/echart-radar)
- 新增图表组件Scatter散点图[在线例子](/doc/echart-scatter)

## 2.0.0
### 2019-05-30
### 说明
- avuex版本开源由内部版本号变更为avue2.0.0

## 1.1.0
### 2019-05-28
### 更新
- 支持最新版的ele组件库(2.8.2+)
- 修复了大量图表组件的bug和优化，同时更新了部分图表组件例子[在线例子](/doc/echart-wordcloud)
- ImagePreview组件内部使用ele的img组件[issues191](https://git.avuejs.com/avue/avuex/issues/191)
- 修复detail组件的row换行问题[issues168](https://git.avuejs.com/avue/avuex/issues/168)
- 修复了组件中部分的国际化问题[issues201](https://git.avuejs.com/avue/avuex/issues/201)
- 优化crud中filter显隐功能替换成el-transfer组件
- 修复form组件中labelWidth为0不起作用的问题
- avuex项目中examples目录新增部分图表组件例子感谢@253495832提供
### 说明
- 为了更好的使用ele的新组件，表格树将会采用ele自带的
- 如果使用ele2.4.9以下版本请使用avuex1.1.0以下版本
- 如果使用ele2.4.9以上的请使用avuex1.1.0(包括1.1.0)以上版本


## 1.0.11
### 2019-05-07
### 更新
- 新增code代码高亮组件[在线例子](/doc/code)
- 新增article文章详情页组件[在线例子](/doc/article)
- 新增comment评论组件[在线例子](/doc/comment)
- 修复type为tree字典的死循环问题和优化样式[issues155](https://git.avuejs.com/avue/avuex/issues/155)
- 修复crud组件cell时remote的url问题[issues183](https://git.avuejs.com/avue/avuex/issues/183)
- 新增行编辑支持remote属性[issues173](https://git.avuejs.com/avue/avuex/issues/173)
- 优化crud组件filterBtn的国际化问题[issues182](https://git.avuejs.com/avue/avuex/issues/182)
- 新增crud组件搜索和清空的配置属性searchSubmitBtn和searchResetBtn
- 新增crud内置方法导出rowExcel和打印rowPrint
- 新增crud属性selectable、selectAll、select事件
- 新增es5对es6的支持包[issues160](https://git.avuejs.com/avue/avuex/issues/160)
- 修复了tree数据量大时报错问题[issues167](https://git.avuejs.com/avue/avuex/issues/167)
- 修复detail组件自定义卡槽不生效问题[issues164](https://git.avuejs.com/avue/avuex/issues/164)
### 说明
- 解决数据字典和字段类型不匹配问题,配置dataType属性(string / number)[在线例子](/doc/form-dataType)
- 新增了大量数据大屏的底层组件（/packages/echart目录下）



## 1.0.10
### 2019-04-10
### 更新
- 新增type为tree时nodeClick事件和checked事件回调获取父类节点数据[issues122](https://git.avuejs.com/avue/avuex/issues/122)
- 修复国际化部分中文没翻译问题[issues124](https://git.avuejs.com/avue/avuex/issues/124),[issues152](https://git.avuejs.com/avue/avuex/issues/152)
- 修复form组件row和offset错位问题[issues136](https://git.avuejs.com/avue/avuex/issues/136)
- 修复tree字典返回$parent父类死循环问题[issues144](https://git.avuejs.com/avue/avuex/issues/144)
- 修复form组件分组时初始化值valueDefault失效问题[issues140](https://git.avuejs.com/avue/avuex/issues/140)
- 修复crud组件动态显隐列时的问题[issues142](https://git.avuejs.com/avue/avuex/issues/142)
- 修复crud组件日期查询是清除数据初始化值问题[issues130](https://git.avuejs.com/avue/avuex/issues/130)
- 修复tabs组件自定义失效问题[issues126](https://git.avuejs.com/avue/avuex/issues/126)
- 修复crud行编辑时maxlength失效问题[issues119](https://git.avuejs.com/avue/avuex/issues/119)
- 修复crud更新option配置时重新渲染搜索栏问题[issues125](https://git.avuejs.com/avue/avuex/issues/125)
- 新增tabs组件的disabled属性[issues141](https://git.avuejs.com/avue/avuex/issues/141)
- 新增type为color颜色选择组件[在线例子](/doc/form-color)
- 新增crud组件行编辑switch属性[在线例子](/doc/crud-cell)
- 新增search组件支持自定义内容[issues115](https://git.avuejs.com/avue/avuex/issues/115),[在线例子](/doc/search)
- 新增数字动画count-up组件,[在线例子](/doc/count-up)
- 修复select远程搜索初始化url错误[issues148](https://git.avuejs.com/avue/avuex/issues/148),[在线例子](/doc/form-select-remote)
- 修复dicUrl错误时字典乱显示问题
- 修复crud复杂表头index序号和其它行乱序问题
- 修复crud不执行fromatter方法问题
- 新增type为cascader时changeOnSelect属性（选择任意一级都可以选择）
### 说明
- 移除type为phone的组件



## 1.0.9
### 2019-03-22
### 更新
## 新增大表哥(宇宙最强表格) [在线例子](/doc/crud-bigcousin)
- 新增crud复杂表头[在线例子](/doc/crud-headers)
- 新增draggable拖拽组件[在线例子](/doc/draggable)
- 新增crud组件的header设置表格头部显隐的属性[issues103](https://git.avuejs.com/avue/avuex/issues/103)
- 修复crud中type为cascader时子集不翻译问题[issues113](https://git.avuejs.com/avue/avuex/issues/113)
- 修复crud中type为tree时联动子类不加载回显问题[issues111](https://git.avuejs.com/avue/avuex/issues/111)
- 新增crud弹出框dialogTop属性
- 调整icon-select组件的的样式问题
- 调整tree组件的弹出样式
- 修复tree组件的验证样式问题
- 扩展form组件的子组件的事件[在线例子](/doc/form-event)
- 新增crud组件弹出dialog窗体自定义表单或分组表单可以和表格列不一致[在线例子](/doc/crud-group)
- 新增crud组件的expand手风琴模式[在线例子](/doc/crud-expand)
- 新增card卡片组件[在线例子](/doc/card)
- 新增input组件属性maxlength提示字符长度提示[在线例子](/doc/form-maxlength)
- 新增crud组件同步element的单元格的点击事件(cell-mouse-enter / cell-mouse-leave / cell-click / cell-dblclick)
- 修复表格树行操作后子类丢失问题[issues89](https://git.avuejs.com/avue/avuex/issues/89)
- 新增back-top组件默认父类不为window，可以根据id属性去指定父类[issues73](https://git.avuejs.com/avue/avuex/issues/73)
- 调整crud组件弹出dialog的样式问题[issues95](https://git.avuejs.com/avue/avuex/issues/95)
- 修复Image-preview组件显示组件的层级问题[issues99](https://git.avuejs.com/avue/avuex/issues/99)
### 说明
- 新增了图表模块，还在开发调试中（请正式发布后再在项目中使用）[在线例子](/doc/echart-bar)

## 1.0.8
### 2019-03-06
### 更新
- 新增empty空状态组件[在线例子](/doc/empty)
- 新增crud组件的空状态使用[在线例子](/doc/crud-empty)
- 新增crud组件typeslot自定义组件参数（目前只有type为select）[在线例子](/doc/crud-select-slot)
- 新增crud组件数据类型dataType方法，主要用于select多选，checkbox，tree等数组组件，无需传入数组，传入逗号隔开的字符串即可
- 新增curd组件expand相关参数[在线例子](/doc/crud-expand)
- 新增crud为tree时checkStrictly属性，不遵循父子规则,也就是勾选父类不会全勾选子类
- 新增form组件分组时自定义分组表头[issues75](https://git.avuejs.com/avue/avuex/issues/75),[在线例子](/doc/form-group)
- 调整crud组件filterBtn默认不显示
- 修复crud组件为icon-select时的样式问题和其他等已知问题[在线例子](/doc/form-icon-select)
- 修复crud动态生成option配置搜索模块不加载问题[#issues92](https://git.avuejs.com/avue/avuex/issues/92)
- 修复drawer抽屉组件属性close-on-click-modal失效问题[#issues91](https://git.avuejs.com/avue/avuex/issues/91)
- 修复crud组件为number时数据的双向绑定问题[#issues88](https://git.avuejs.com/avue/avuex/issues/88)
- 修复crud组件设置hide无法生效问题[#issues87](https://git.avuejs.com/avue/avuex/issues/87)
- 修复form组件动态设置规则时双向绑定问题[#issues84](https://git.avuejs.com/avue/avuex/issues/84)
- 修复crud核心方法不执行的问题[#issues83](https://git.avuejs.com/avue/avuex/issues/83)
- 扩展crud组件为number时数据类型[#issues78](https://git.avuejs.com/avue/avuex/issues/78)
### 说明
- curd组件为switch初始化值时的问题[#issues86](https://git.avuejs.com/avue/avuex/issues/86)
- 新增全局配置属性文档[在线文档](/doc/window)


## 1.0.7
### 2019-02-26
### 更新
- 修复form组件的gutter栏间距属性[在线例子](/doc/form-gutter)
- 修复form组件分组是rules不加载的问题
- 新增全局方法setPx内部方法[全局api](/doc/api)
- 新增骨架屏skeleton组件[在线例子](/doc/skeleton)
- 新增头像avatar组件[在线例子](/doc/avatar)
- 新增分割线divider组件[在线例子](/doc/divider)
- crud组件新增dialogHeight属性，配置弹出框的高度，同时将滚动条换成el-scrollbar组件
- crud组件修复table配置项目keyId的rowKey问题[#issues76](https://git.avuejs.com/avue/avuex/issues/76)
- crud组件修复table中filter值不实时更新问题[#issues70](https://git.avuejs.com/avue/avuex/issues/70)
- crud组件修复导出按钮国际化问题[#issues70](https://git.avuejs.com/avue/avuex/issues/69)
- crud组件中自定义卡槽时新增字典翻译label属性
- tree组件增加size属性[#issues81](https://git.avuejs.com/avue/avuex/issues/81),[在线例子](/doc/tree)
- form组件中v-model对象自带字典翻译$属性
- upload组件修复https域名下七牛云无法上传问题
- tree组件修复defaultExpandAll全部展开属性无法生效问题

## 1.0.6
### 2019-02-12
### 更新
- 全局api方法$log变更为$Log方法[全局api](/doc/api)
- icon-select组件无法双向绑定问题[#issues66](https://git.avuejs.com/avue/avuex/issues/66),[在线例子](/doc/form-icon-select)
- crud新增国际化和按钮权限控制功能[#issues65](https://git.avuejs.com/avue/avuex/issues/66),[权限例子](/doc/crud-permission),[国际化例子](/doc/i18n)
- upload组件新增阿里云oss功能[在线例子](/doc/form-upload-ali)
- upload组件新增七牛云oss功能[在线例子](/doc/form-upload-qiniu)
- 新增type为search时的搜索组件[在线例子](/doc/form-search)
- 新增图片预览image-preview组件[在线例子](/doc/image-preview)
- 新增剪切板全局api[在线例子](/doc/clipboard)
- 废除table-tree组件将crud组件扩展支持tree[#issues62](https://git.avuejs.com/avue/avuex/issues/66),[#issues59](https://git.avuejs.com/avue/avuex/issues/59),[在线例子](/doc/crud-tree)
- 完善脚手架内容和修复其他问题[#pr56](https://git.avuejs.com/avue/avuex/pulls/56)
### 说明
- 当配置type为number时，验证无法通过问题[#issues58](https://git.avuejs.com/avue/avuex/issues/58)
- crud中使用筛选时遇到的问题[#issues68](https://git.avuejs.com/avue/avuex/issues/68),[在线例子](/doc/crud-filters)


## 1.0.5
### 2019-01-29
### 更新
- form新增validateField方法[#issues55](https://git.avuejs.com/avue/avuex/issues/55)
- 优化cascader级联逻辑[#issues52](https://git.avuejs.com/avue/avuex/issues/52)
- 新增tree-table中menu的控制属性[#issues50](https://git.avuejs.com/avue/avuex/issues/50)
- 新增drawer抽屉组件[在线例子](/doc/drawer)
- 新增back-top返回顶部组件[在线例子](/doc/back-top)
- 新增affix图钉组件[在线例子](/doc/affix)
- 新增timeline时间轴组件[在线例子](/doc/timeline)
- 新增text-ellipsis超出文本省略组件[在线例子](/doc/ext-ellipsis)
- 新增icon-select图标选择器组件[在线例子](/doc/form-icon-select)
- 新增数据展示模版[数据展示11](/doc/data11)
- 全部数据展示模版新增click和href属性
### 说明
- 使用expand属性时必须配置idKey属性为你行数据的主键，不能重复[在线例子](/doc/crud-expand)[#issues51](https://git.avuejs.com/avue/avuex/issues/51)


## 1.0.4
### 2019-01-19
### 更新
- form组件number时验证时的样式问题（同时也是element-ui原生的问题）[#issues39](https://git.avuejs.com/avue/avuex/issues/39)
- 优化upload组件上传附加时点击非展示而是下载[#issues44](https://git.avuejs.com/avue/avuex/issues/44),[#issues47](https://git.avuejs.com/avue/avuex/issues/47)
- 修复tabs组件时部分属性失效问题[#issues45](https://git.avuejs.com/avue/avuex/issues/45)
- 修复tabs组件数据双向绑定问题[#issues46](https://git.avuejs.com/avue/avuex/issues/46)
- 修复search组件的双向绑定造成卡死问题[在线例子](/doc/search)
- 优化crud组件tip部分，同时开放了tip相关参数[在线例子](/doc/crud-tip)
- 调整数据展示组件部分模版[数据展示7](http://localhost:8080/doc/data7),[数据展示9](http://localhost:8080/doc/data9)
- 调整axios参数将$http变为$axios方法，解决与其他框架重名问题
### 说明
- 动态字典的dicData问题[#issues30](https://git.avuejs.com/avue/avuex/issues/30),[#issues40](https://git.avuejs.com/avue/avuex/issues/40)
- 如果定义卡槽的问题[#issues42](https://git.avuejs.com/avue/avuex/issues/42)
- 关于valueDefault属性问题[#issues35](https://git.avuejs.com/avue/avuex/issues/35)


## 1.0.3
### 2019-01-14
### 更新
- crud组件修复编辑时候无法返回行index问题[#issues36](https://git.avuejs.com/avue/avuex/issues/36)
- crud组件新增手机端操作栏宽度属性menuXsWidth,默认宽度为100[#issues34](https://git.avuejs.com/avue/avuex/issues/34)
- curd组件手机端分页样式的调整[#issues29](https://git.avuejs.com/avue/avuex/issues/29)
- upload组件修复各种问题，完善优化逻辑[#issues31](https://git.avuejs.com/avue/avuex/issues/31)
- upload组件新增canvasOption属性,用于配置上传时水印(基于[avue-canvas](https://canvas.avuejs.com))和图片压缩配置具体参考[在线例子](/doc/form-img),提供了在线图片接口，大家可以上传图片测试
- upload组件上传前后调用before-close和upload-before方法失效问题[#issues26](https://git.avuejs.com/avue/avuex/issues/26)
- upload组件修复弹出大图是dialog涂层问题
- form组件新增gutter(项之间的间距默认为20)和offset(当前项的偏移量)属性
### 说明
- 在动态赋值dicData字典时,需要在数组中先申明dicData为空数组用于监听数据[#issues30](https://git.avuejs.com/avue/avuex/issues/30)
- 所有option中dicData方法废除,需要直接赋值数组字典到column的dicData中
#### 感谢贡献
- [Zclhlmgqzc](https://github.com/Zclhlmgqzc)
- [黑夜](https://github.com/oorzc)
- [feebool](https://github.com/6313504)


## 1.0.2
### 2019-01-10

- crud组件手机版本分页太长的样式问题[#issues29](https://git.avuejs.com/avue/avuex/issues/29)
- form组件中upload组件上传图片回掉方法失效问题[#issues26](https://git.avuejs.com/avue/avuex/issues/26)
- curd组件表格字典不翻译问题[#issues24](https://git.avuejs.com/avue/avuex/issues/24)
- 优化form组件的级联逻辑[#issues20](https://git.avuejs.com/avue/avuex/issues/20)
- 增强props和httpProps字典和图片上传res字段自定义网络结构属性,比如返回一个对象(res:'data.data'),如果返回一个数组中的对象(res:'data.0)
- crud组件新增监听dicData字典数据动态赋值
- 数据展示组件全部新增click点击方法


## 1.0.1
### 2019-01-07

- crud组件searchChange回调，返回参数过滤空字段[#issues16](https://git.avuejs.com/avue/avuex/issues/16)
- crud组件filtetBtn变更为filtertBtn属性[#issues14](https://git.avuejs.com/avue/avuex/issues/14)
- crud组件和form组件中visdiplay变更为display属性[#issues12](https://git.avuejs.com/avue/avuex/issues/12)
- crud组件调整头部样式布局[1](https://git.avuejs.com/avue/avuex/issues/1)
- crud组件中addVisdiplay,editVisdiplay,viewVisdiplay变更为addDisplay,editDisplay,viewDisplay属性[#issues12](https://git.avuejs.com/avue/avuex/issues/12)
- form组件分组新增display属性[#issues9](https://git.avuejs.com/avue/avuex/issues/9)
- form组件去掉upload时tip重复问题[#issues8](https://git.avuejs.com/avue/avuex/issues/8)
- select组件新增配置tags属性控制显示方式[#issues6](https://git.avuejs.com/avue/avuex/issues/6)
- upload组件新增accept(文件类型,字符串或数组),filesize(文件大小kb为单位)[#issues7](https://git.avuejs.com/avue/avuex/issues/7)
- 网络字典和图片上传可配置返回数据结构属性props和httpProps中res属性,如何没有配置默认以前的规则

## 1.0.0
### 2019-01-01

avuex发布，具体变更看[变更说明](/doc/explain)

