# 变更说明

由于Avue2.0还是基于avue底层优化升级而来，所以兼容旧版本的几乎全部功能，在现有项目中集成旧的可达到微乎其微的变动去升级，

## 为什么要有变动？

经过在avue中不断的版本迭代我们逐步发现了一些问题，存在大量可以优化的地方，以及一些配置的烦琐性，对企业的支持功能少之更少，你可以不采用新特性而去用旧版本也没有任何问题，同时也感谢你对avue的支持


### 属性变动
- solt变更成slot

- formsolt变更成formslot
- searchsolt去除，只要定义search卡槽就会开启搜索相关的功能
- cascaderFirst去除，cascader变更成cascaderItem[表单级联文档](/doc/form-select)
- showClomnu变更为showColumn是否加入控制动态显隐列
- 网络字典使用时不必填写dicData,当然使用本地字典时候还是需要填写dicData
- visdiplay变更为display
- addVisdiplay变更为addDisplay
- editVisdiplay变更为editDisplay
- viewVisdiplay变更为viewDisplay

### 组件变动
- date-group组件去除
- avue-form-detail组件变成为avue-detail组件，配置属性也发生变化,具体参考[detail详情页文档](/doc/detail)
- 子组件中去掉crud关键字(例如avue-crud-input -> avue-input，avue-crud-select -> avue-select的类似)
- steps组件去除
- tabs组件变更，具体参考[tabs选项卡文档](/doc/tabs)
- json-tree组件去除
